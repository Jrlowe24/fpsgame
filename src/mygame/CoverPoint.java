/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.effect.ParticleEmitter;
import com.jme3.material.Material;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Sphere;

/**
 *
 * @author James
 */
public class CoverPoint extends Node {

    private Enemy nearestEnemy;
    private boolean isLOS; //Line of Sight; true if has direct visual of enemy 
    private boolean isCover;
    private Vector3f playerLoc;
    Node rootNode;
    Ray LOScheck = new Ray();
    Ray Covercheck =  new Ray();
    
    CollisionResults results = new CollisionResults();
    Vector3f eyeHeight = new Vector3f(0, 5.5f, 0);
    Vector3f kneeHeight = new Vector3f(0, 1.5f, 0);
    Sphere s = new Sphere(16, 16, 1f);
    Geometry sphere = new Geometry("GEo", s);

    public CoverPoint(Vector3f loc, Node rootNode, Material test) {
        setLocalTranslation(loc.x, 0, loc.z);
        LOScheck.setOrigin(new Vector3f(loc.x,eyeHeight.y,loc.z));
        LOScheck.setDirection(Vector3f.ZERO);
        Covercheck.setOrigin(new Vector3f(loc.x,kneeHeight.y,loc.z));
        Covercheck.setDirection(Vector3f.ZERO);
        this.attachChild(sphere);
        sphere.setMaterial(test);
        sphere.setLocalTranslation(this.getWorldTranslation());
        this.rootNode = rootNode;
        rootNode.attachChild(sphere );
        
    }

    public void Update(float tpf) {
        if (playerLoc != null) {
            Vector3f dir = playerLoc.subtract(getWorldTranslation());
            dir.y = 0;
            //Vector3f dir = getWorldTranslation().subtract(playerLoc);
            LOScheck.setDirection(dir);
            Covercheck.setDirection(dir);
            //System.out.println("Direction " + dir);
           // System.out.println("Is cover? " + isCover);
             findLOS();
             findCover();
             
        }
       
    }
    float Distance =0;
    public void findLOS() {
         for (Spatial s : rootNode.getChildren()) {
            if (!(s instanceof Enemy)){
                if (!(s instanceof ParticleEmitter)){
                    if (s.getName() != "gun"){
                    //System.out.println(" name " + s.getName());
                // rootNode.getChild("Player").collideWith(LOScheck, results);
                s.collideWith(LOScheck, results);
                    }
                }
            }
            
        }
        
        if (results.size() > 0){
        
        CollisionResult object = results.getClosestCollision();
        float distance = object.getDistance();
        String target = object.getGeometry().getName();
      //  System.out.println("LOS tar " + target);
        //System.out.println(" isLOS " + isLOS + " isCover? " + isCover);
        Distance = distance;

        if ("Body".equals(target) || "Head".equals(target) ||  "Legs".equals(target)){
            isLOS = true;
        }
        else{
            isLOS = false;
        }
        
        }
        results.clear();

    }
    
    public void findCover(){
         for (Spatial s : rootNode.getChildren()) {
            if (!(s instanceof Enemy)){
                if (!(s instanceof ParticleEmitter)){
                    if (s.getName() != "gun"){
                // rootNode.getChild("Player").collideWith(LOScheck, results);
                s.collideWith(Covercheck, results);
                    }
                }
            }
            
        }
        
        if (results.size() > 0){
        
        CollisionResult object = results.getClosestCollision();
        float distance = object.getDistance();
        String target = object.getGeometry().getName();
        //System.out.println(" cover tar " + target);
        Distance = distance;

        if ("Body".equals(target) || "Head".equals(target) ||  "Legs".equals(target)){
            isCover = false;
        }
        else if (distance < 5){
            isCover = true;
        }
        
        }
        results.clear();

        
    }

    public float getDistance() {
        return Distance;
    }

    public Vector3f getPlayerLoc() {
        return playerLoc;
    }

    
    
    public void setPlayerLoc(Vector3f loc) {
        playerLoc = loc;
    }

    public boolean isCover() {
        return isCover;
    }

    public boolean isLOS() {
        return isLOS;
    }

    public Enemy nearestEnemy() {
        return nearestEnemy;
    }
}
