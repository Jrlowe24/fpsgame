/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.ui.Picture;

/**
 *
 * @author James
 */
public class MiniMap {
    
    Camera camera; 
    CharacterControl target;
    Picture arrow;
    Camera cam1;
    
    public MiniMap(Camera cam, RenderManager rm, Node map, CharacterControl player, Node guiNode, AssetManager assetManager){
    camera = cam.clone();
    camera.setViewPort(.02f, .22f, .73f, .98f);
    ViewPort Minimap = rm.createMainView("MiniMap", camera);
    Minimap.attachScene(map);
    target = player;
    arrow = new Picture("Arrow");
    arrow.setImage(assetManager, "Textures/Arrow.png", true);
    arrow.setPosition(140, 600);
    arrow.setWidth(30);
    arrow.setHeight(30);
    guiNode.attachChild(arrow);
    cam1 = cam;
    camera.setLocation(new Vector3f(target.getPhysicsLocation().getX(),target.getPhysicsLocation().getY()+70,target.getPhysicsLocation().getZ()));
    //camera.lookAt(target.getPhysicsLocation(), Vector3f.UNIT_Y);
    //camera.setRotation(new Quaternion().fromAngles(1, 0, 0));
    
   }
    Quaternion rot = new Quaternion();
    Quaternion cameraRot = new Quaternion();
    public void Update(float tpf){
        camera.setLocation(new Vector3f(target.getPhysicsLocation().getX(),target.getPhysicsLocation().getY()+70,target.getPhysicsLocation().getZ()));
        //camera.lookAt(target.getPhysicsLocation(), Vector3f.UNIT_Y);
        //camera.setRotation(new Quaternion(camera.getRotation().getX(),cam1.getDirection().z, camera.getRotation().getZ(), 1.0f));
        //cameraRot.set(camera.getRotation().getX(), camera.getRotation().getY(), cam1.getRotation().getY(), camera.getRotation().getW());
        //cameraRot.set(camera.getRotation().getX(), cam1.getRotation().getZ(), camera.getRotation().getZ(), camera.getRotation().getW());
         //cameraRot.set(camera.getRotation().getX(), camera.getRotation().getY(), camera.getRotation().getZ(), camera.getRotation().getW());
      //  cameraRot.set(camera.getRotation().getX(), camera.getRotation().getY(), camera.getRotation().getZ(), camera.getRotation().getW());
     
        // camera.setRotation(cameraRot);
        //System.out.println("MiniCam " + camera.getRotation() + "MainCam " + cam1.getRotation());
        //camera.getRotation().set(camera.getRotation().getX(), cam1.getDirection().x, camera.getRotation().getZ(), camera.getRotation().getW());
        //System.out.println("MiniCam " + camera.getRotation() + "MainCam " + cam1.getDirection());
        //camera.lookAt(new Vector3f(target.getPhysicsLocation().getX(),cam1.getDirection().x,target.getPhysicsLocation().getZ()), Vector3f.UNIT_Y);
     
        //arrow.rotate(0, 0, cam1.getRotation().getY());
        //rot.set(0, 0, cam1.getRotation().getY(), 0);
        //arrow.setLocalRotation(rot);
        
    }
    public Camera getMiniCam(){
        return camera;
    }
    
}
