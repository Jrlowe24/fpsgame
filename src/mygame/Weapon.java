/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.AnimEventListener;
import com.jme3.animation.LoopMode;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.util.Random;

/**
 *
 * @author James
 */
public class Weapon implements ActionListener, AnalogListener, AnimEventListener {

        Vector3f IronSightsAimLoc;
        Vector3f AcogSightsAimLoc;
        String sightType;
        Vector3f HipLoc;
        Vector3f runLoc;
        Quaternion runQuat;
        Quaternion WalkQuat = new Quaternion(0, 0, 0, 1);
        Node gunHolder = new Node("GunHolder");
        Node Bolt;
        public float ROF;
        float weight;
        int numBulletsinClip;
        int TotalBullets;
        int currentAmmo;
        int clipSize;
        float sightSpeed;
        Node muzzle;
        Node weapon;
        Node ejectionPort;
        boolean Shoot = false, Aim = false, Left = false, Right = false, Up = false, Back = false, Run = false;
        float recoilspeed;
        float recoilback;
        InputManager inputManager;
        float delay;
        Vector3f recoilLoc;
        float Recoilz;
        AudioNode sound;
        boolean burst;
        int burstNum;
        AnimChannel channel;
        AnimControl control;
        ParticleEmitter ejection = new ParticleEmitter("Ejection", ParticleMesh.Type.Triangle, 3);
        public float xSwayAmount = 0.05F;
        public float ySwayAmount = 0.05F;
        public float maxXAmount;
        public float maxYAmount;
        public Vector3f orig;
        public float smooth = 3F;
        float SwayAmount = .05f;
        float SwaySpeed = 5f;
        Quaternion currenCamRecoil = new Quaternion();
        Material shell;
        Node ACOG;
        Node IRON;
        Node REDDOT;
        Node currentScope;
        float zoomAmount;
        float AcogzoomAmount;
        float currentZoom = 0;
        Spatial Hands;
        RigidBodyControl weaponControl = new RigidBodyControl(1f);
        String name;
        Node gun;
        ParticleEmitter spark1 = new ParticleEmitter("spark", ParticleMesh.Type.Triangle, 10);
        ParticleEmitter flash = new ParticleEmitter("spark", ParticleMesh.Type.Triangle, 2);
        ParticleEmitter smoke = new ParticleEmitter("spark", ParticleMesh.Type.Triangle, 10);
        ParticleEmitter spark = new ParticleEmitter("spark", ParticleMesh.Type.Triangle, 20);

        public Weapon(AssetManager assetManager, Node weapon, String Scopefilename, String sighttype, String name) {
                this.weapon = weapon;
               
                gun = (Node) weapon.getChild("Gun");
                Hands = weapon.getChild("Hands");
                if (Scopefilename != null) {
                        currentScope = (Node) assetManager.loadModel("Models/Guns/Scopes/" + Scopefilename + ".j3o");
                      //  TangentBinormalGenerator.generate(currentScope);
                      //  TangentBinormalGenerator.generate(weapon);
                        gun.attachChild(currentScope);
                        currentScope.setLocalTranslation(Vector3f.ZERO);
                        sightType = sighttype;
                        System.out.println(" Type " + sightType);
                        if ("ACOG".equals(sightType)) {
                                currentZoom = AcogzoomAmount;
                        }
                        if ("IRON".equals(sightType)) {
                                currentZoom = zoomAmount;
                        }
                        //currentScope.setLocalTranslation(weapon.getChild("ScopeMount").getLocalTranslation());
                } else {
                        sightType = "IRON";
                        currentZoom = zoomAmount;
                }
                ejection.setStartColor(new ColorRGBA(1f, 0.4f, 0.0f, 1.0f));
                ejection.setEndColor(new ColorRGBA(1f, 0.5f, 0.0f, 1.0f));
                ejection.setStartSize(.15f);
                ejection.setEndSize(.15f);
                ejection.setFacingVelocity(true);
                ejection.setParticlesPerSec(0);
                ejection.setGravity(0, 5, 0);
                ejection.setLowLife(.05f);
                ejection.setHighLife(.1f);
                ejection.setImagesX(1);
                ejection.setImagesY(1);



                ejection.getParticleInfluencer().setVelocityVariation(.07f);
                Material mat = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
                mat.setTexture("Texture", assetManager.loadTexture("Effects/Explosion/spark.png"));
                ejection.setMaterial(mat);

                

                weapon.attachChild(ejection);
                System.out.println("Parent1 " + ejection.getParent());
                ejection.setInWorldSpace(false);
                //System.out.println(ejection.getParent());
                //ejection.getParticleInfluencer().setInitialVelocity(new Vector3f(7.0f, 2.5f, 0));
               // Spatial an = weapon.getChild("2");

                // control = an.getControl(AnimControl.class);
                // control.addListener(this);
                //channel = control.createChannel();
                weapon.addControl(weaponControl);
                com.jme3.bullet.collision.shapes.CollisionShape cs = CollisionShapeFactory.createDynamicMeshShape(gun);
                weaponControl.setCollisionShape(cs);
                weaponControl.setEnabled(false);
               // channel.setAnim("Reload");
               // System.out.println("Animations " + channel.getAnimationName());
                this.name = name;
                //  if (name == "M4A1 w/ Iron"){
                //TangentBinormalGenerator.generate(weapon);

                //weapon.setMaterial(mat);
              //  weapon.setShadowMode(RenderQueue.ShadowMode.Receive);
                setUpParticles(assetManager);

        }


        public void addPhysicsWeapon(BulletAppState bas) {
                bas.getPhysicsSpace().add(weaponControl);
                weaponControl.getPhysicsSpace().disableDebug();
        }

        public void Keys() {

                inputManager.addMapping("Shoot", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
                inputManager.addMapping("Aim", new MouseButtonTrigger(MouseInput.BUTTON_RIGHT));
                inputManager.addMapping("Aim", new MouseButtonTrigger(MouseInput.BUTTON_RIGHT));
                inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_A));
                inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_D));
                inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_W));
                inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_S));
                inputManager.addMapping("Run", new KeyTrigger(KeyInput.KEY_LSHIFT));
                inputManager.addMapping("MouseLeft", new MouseAxisTrigger(MouseInput.AXIS_X, true));
                inputManager.addMapping("MouseRight", new MouseAxisTrigger(MouseInput.AXIS_X, false));
                inputManager.addMapping("MouseUp", new MouseAxisTrigger(MouseInput.AXIS_Y, true));
                inputManager.addMapping("MouseDown", new MouseAxisTrigger(MouseInput.AXIS_Y, false));
                inputManager.addMapping("Reload", new KeyTrigger(KeyInput.KEY_R));
                inputManager.addListener(this, "Shoot");
                inputManager.addListener(this, "Aim");
                inputManager.addListener(this, "Left");
                inputManager.addListener(this, "Right");
                inputManager.addListener(this, "Up");
                inputManager.addListener(this, "Down");
                inputManager.addListener(this, "MouseLeft");
                inputManager.addListener(this, "MouseRight");
                inputManager.addListener(this, "MouseUp");
                inputManager.addListener(this, "MouseDown");
                inputManager.addListener(this, "Run");
                inputManager.addListener(this, "Reload");



        }
        float mousex = 0;
        float mousey = 0;
        float headbobStepCounter;
        float gunbobSpeed = 9.0f;
        float gunbobAmountX;
        float gunbobAmountY;
        float currentgunbobX;
        float currentgunbobY;
        float raciogunhold;
        float runTransition = 15;
        Vector3f CurrentRecoil1 = new Vector3f();
        Vector3f CurrentRecoil2 = new Vector3f();
        Vector3f CurrentRecoil3 = new Vector3f();
        Vector3f CurrentRecoil4 = new Vector3f();
        Vector3f RecoilKickback;
        Vector3f AimRecoilKickback;
        Vector3f RecoilRotation;
        Vector3f AimRecoilRotation;
        Node RecoilHolder = new Node("RecoilHolder");
        Vector3f camRecoilRotation;
        float camRecoilSpeed;
        Vector3f CurrentBoltLoc1 = new Vector3f();
        Vector3f CurrentBoltLoc2 = new Vector3f();
        float boltSpeed;
        int currentShot = 3;

        public void update(float tpf) {
                if (weapon != null) {
                        // System.out.println(tpf);
                        headbobStepCounter += tpf * gunbobSpeed;
                        currentgunbobX = (float) Math.sin(headbobStepCounter) * gunbobAmountX * raciogunhold;
                        currentgunbobY = (float) Math.cos(headbobStepCounter * 2) * -gunbobAmountY * raciogunhold;
                        weapon.getLocalTranslation().set(weapon.getLocalTranslation().x + currentgunbobX, weapon.getLocalTranslation().y + currentgunbobY, weapon.getLocalTranslation().z);

                        if (Up || Back || Left || Right) {
                                raciogunhold = 1.0f;
                        } else {
                                raciogunhold = 0;
                        }

                        if (!Left || !Right) {
                        }

                        delay += tpf * ROF;

                        float fx = -mousex * xSwayAmount;
                        float fy = -mousey * ySwayAmount;

                        if (fx > maxXAmount) {
                                fx = maxXAmount;
                        }
                        if (fx < -maxXAmount) {
                                fx = -maxXAmount;
                        }
                        if (fy > maxYAmount) {
                                fy = maxYAmount;
                        }
                        if (fy < -maxYAmount) {
                                fy = -maxYAmount;
                        }

                        Vector3f newPos = new Vector3f(orig.x + fx, orig.y + fy, orig.z);
                        weapon.getLocalTranslation().interpolate(newPos, tpf * smooth);
                        mousex = 0;
                        mousey = 0;
                        //System.out.println("Hip Loc " + HipLoc);

                        if (Aim) {
                                gunbobAmountX = .001f;
                                gunbobAmountY = .001f;
                                maxXAmount = .015f;
                                maxYAmount = .015f;
                                gunbobSpeed = 6.0f;
                                SwayAmount = .008f;
                                if (sightType == "IRON") {
                                        gunHolder.getLocalTranslation().interpolate(IronSightsAimLoc, sightSpeed * tpf);
                                } else if (sightType == "ACOG") {
                                        gunHolder.getLocalTranslation().interpolate(AcogSightsAimLoc, sightSpeed * tpf);
                                }
                                gunHolder.getLocalRotation().slerp(WalkQuat, runTransition * tpf);
                                if (Left) {
                                        weapon.getLocalRotation().slerp(new Quaternion(0, 0, weapon.getLocalRotation().getZ() - SwayAmount, 1), tpf * SwaySpeed);
                                }
                                if (Right) {
                                        weapon.getLocalRotation().slerp(new Quaternion(0, 0, weapon.getLocalRotation().getZ() + SwayAmount, 1), tpf * SwaySpeed);
                                }
                                if (!Left || !Right) {
                                        weapon.getLocalRotation().slerp(new Quaternion(0, 0, 0, 1), tpf * SwaySpeed);

                                }

                        } else {
                                gunbobAmountX = .008f;
                                gunbobAmountY = .006f;
                                maxXAmount = .1f;
                                maxYAmount = .06f;
                                SwayAmount = .05f;
                                if (Run && Up) {

                                        gunbobSpeed = 13.0f;
                                        gunHolder.getLocalTranslation().interpolate(runLoc, runTransition * tpf);
                                        gunHolder.getLocalRotation().slerp(runQuat, runTransition * tpf);

                                } else {
                                        gunbobSpeed = 9.0f;
                                        gunHolder.getLocalTranslation().interpolate(HipLoc, sightSpeed * tpf);
                                        gunHolder.getLocalRotation().slerp(WalkQuat, sightSpeed * tpf);
                                        if (Left) {
                                                weapon.getLocalRotation().slerp(new Quaternion(0, 0, weapon.getLocalRotation().getZ() - SwayAmount, 1), tpf * SwaySpeed);
                                        }
                                        if (Right) {
                                                weapon.getLocalRotation().slerp(new Quaternion(0, 0, weapon.getLocalRotation().getZ() + SwayAmount, 1), tpf * SwaySpeed);
                                        }
                                        if (!Left || !Right) {
                                                weapon.getLocalRotation().slerp(new Quaternion(0, 0, 0, 1), tpf * SwaySpeed);

                                        }
                                }
                        }

                        Recoil(tpf);


                        CurrentRecoil1.interpolate(CurrentRecoil1, Vector3f.ZERO, .2f); //.2
                        CurrentRecoil2.interpolate(CurrentRecoil2, CurrentRecoil1, .3f); //.3

                        CurrentRecoil3.interpolate(CurrentRecoil3, Vector3f.ZERO, .2f); //.2
                        CurrentRecoil4.interpolate(CurrentRecoil4, CurrentRecoil3, .6f); //.4

                        RecoilHolder.setLocalTranslation(CurrentRecoil4);
                        RecoilHolder.setLocalRotation(new Quaternion(CurrentRecoil2.x, CurrentRecoil2.y, CurrentRecoil2.z, 1));
                        //cameraNode.getLocalRotation().slerp(currenCamRecoil, tpf * camRecoilSpeed); 
                        if (Bolt != null) {
                                CurrentBoltLoc1.interpolate(CurrentBoltLoc1, Vector3f.ZERO, .55f); //.2
                                CurrentBoltLoc2.interpolate(CurrentBoltLoc2, CurrentBoltLoc1, .8f); //.4
                                Bolt.setLocalTranslation(CurrentBoltLoc2);

                        }
                        // System.out.println(" Recoild Holder " + RecoilHolder.getLocalTranslation());
                }
              //  System.out.println("  Shooting " + Shoot);

        }
        float shootTime;

        public void Recoil(float tpf) {
                if (currentAmmo > 0) { //if theres ammo
                        if (Shoot && !Run) { //if you are shooting and not running
                                if (shootTime <= 0) { //if can shoot
                                        if (currentShot > 0) { //if mag isnt empt
                                                if (burstNum != 1) { //if in burst mode
                                                        currentShot--;
                                                }
                                                if (Aim) {

                                                        CurrentRecoil3.set(CurrentRecoil3.x + Range(-AimRecoilKickback.x, AimRecoilKickback.x), CurrentRecoil3.y + Range(-AimRecoilKickback.y, AimRecoilKickback.y), AimRecoilKickback.z + AimRecoilKickback.z);
                                                        CurrentRecoil1.set(CurrentRecoil1.x, CurrentRecoil1.y + Range(-AimRecoilRotation.y, AimRecoilRotation.y), CurrentRecoil1.z);

                                                } else {


                                                        CurrentRecoil1.set(CurrentRecoil1.x, CurrentRecoil1.y + Range(-RecoilRotation.y, RecoilRotation.y), CurrentRecoil1.z);
                                                        CurrentRecoil3.set(CurrentRecoil3.x + Range(-RecoilKickback.x, RecoilKickback.x), CurrentRecoil3.y + Range(-RecoilKickback.y, RecoilKickback.y), CurrentRecoil3.z + RecoilKickback.z);
                                                        // currenCamRecoil.set(cameraNode.getLocalRotation().getX() + Range(-camRecoilRotation.x, camRecoilRotation.x),  cameraNode.getLocalRotation().getY() + Range(-camRecoilRotation.y, camRecoilRotation.y),  cameraNode.getLocalRotation().getZ(), 1.0f);
                                                }
                                                Shoot();
                                                if (currentAmmo > 1) {
                                                        currentAmmo--;
                                                } else {
                                                        TotalBullets = TotalBullets - clipSize;
                                                        if (TotalBullets >= 30) {
                                                                currentAmmo = clipSize;
                                                        } else {
                                                                currentAmmo = TotalBullets;
                                                        }
                                                }
                                            //    System.out.println("TotalAmmo " + TotalBullets + "ClipAmmo" + currentAmmo);
                                                if (Bolt != null) {
                                                        CurrentBoltLoc1 = new Vector3f(0, 0, -.5f);

                                                }
                                                shootTime = 1;
                                                ejection.killAllParticles();
                                                weapon.attachChild(ejection);
                                                //ejection.setLocalTranslation(ejectionPort.getLocalTranslation());
                                                ejection.setLocalTranslation(ejectionPort.getLocalTranslation());
                                                //ejection.getParticleInfluencer().setInitialVelocity(new Vector3f(7.0f, 2.5f, 0));
                                                ejection.getParticleInfluencer().setInitialVelocity(ejectionPort.getLocalTranslation().add(-7.0f, 2.5f, 0));
                                                ejection.emitAllParticles();

                                        }
                                        
                                }

                        }
                        shootTime -= tpf * ROF;

                } else { // if currentAmmo is 0;
                        reload();
                }
                if (currentShot == 0){ //if burst mode finished.
                        Shoot = false;
                        currentShot = burstNum;
                }
        }

        public void reload() {
                  //channel.setAnim("Reload", 0.50f);
                 // channel.setLoopMode(LoopMode.DontLoop);
                 // channel.setSpeed(0.5f);
                //  currentAmmo = 30;
        }

        public void Shoot() {
                sound.playInstance();
                muzzleflash();
        }
        
        public void muzzleflash() {

                spark1.setLocalTranslation(muzzle.getWorldTranslation());
               // spark1.emitAllParticles();


                //  rootNode.attachChild(flash);
                flash.setLocalTranslation(muzzle.getWorldTranslation());
                flash.emitAllParticles();

                //smoke.setStartColor(new ColorRGBA(0f, 0f, 0f, .3f));
                //smoke.setEndColor(new ColorRGBA(.1f, .1f, .1f, 0));

                smoke.setLocalTranslation(muzzle.getWorldTranslation());
                smoke.emitAllParticles();

        }


        @Override
        public void onAction(String name, boolean isPressed, float tpf) {
                if (name.equals("Shoot") && isPressed) {
                        Shoot = true;

                }
                if (name.equals("Shoot") && !isPressed && burstNum == 1) {
                        Shoot = false;
                       // currentShot = burstNum;

                } else if (name.equals("Aim")) {
                        Aim = isPressed;
                } else if (name.equals("Left")) {
                        Left = isPressed;
                } else if (name.equals("Right")) {
                        Right = isPressed;
                } else if (name.equals("Up")) {
                        Up = isPressed;
                } else if (name.equals("Down")) {
                        Shoot = false;
                        currentShot = 3;
                        Back = isPressed;
                } else if (name.equals("Run")) {

                        Run = isPressed;
                } else if (name.equals("Reload")) {
                        reload();

                }
                if (name.equals("Reload") && isPressed){
           // if (!channel.getAnimationName().equals("Reload")){
               // channel.setAnim("Reload", 0);
               // channel.setLoopMode(LoopMode.DontLoop);
                //channel.setSpeed(1.0f);
            //}
        }
        }

        public void setInputManager(InputManager im) {
                inputManager = im;
                Keys();
        }
        boolean swaying;

        @Override
        public void onAnalog(String name, float value, float tpf) {
                if (name.equals("MouseLeft")) {
                        mousex = value * 40;
                        swaying = true;

                } else if (name.equals("MouseRight")) {
                        mousex = value * -40;
                        swaying = true;
                }
                if (name.equals("MouseUp")) {
                        mousey = value * -40;
                        swaying = true;

                } else if (name.equals("MouseDown")) {
                        mousey = value * 40;
                        swaying = true;
                } else if (name.equals("Right")) {
                        //System.out.println("rightspeed " + value);
                }

                //mousex = 0;
                // }


        }

        @Override
        public void onAnimCycleDone(AnimControl control, AnimChannel channel, String animName) {
                if (animName.equals("Reload")){
            channel.setAnim("Reload", 0.5f);
            channel.setLoopMode(LoopMode.DontLoop);
            channel.setSpeed(.5f);
                }
        }

        @Override
        public void onAnimChange(AnimControl control, AnimChannel channel, String animName) {
               
                    
           //channel.setAnim("Reload", 0.50f);
            //channel.setLoopMode(LoopMode.DontLoop);
            //channel.setSpeed(.05f);
        }
        

        public static float SmoothDamp(float current, float target, float currentVelocity, float smoothTime, float maxSpeed, float deltaTime) {
                smoothTime = Max(0.0001f, smoothTime);
                float num = 2f / smoothTime;
                float num2 = num * deltaTime;
                float num3 = 1f / (1f + num2 + 0.48f * num2 * num2 + 0.235f * num2 * num2 * num2);
                float num4 = current - target;
                float num5 = target;
                float num6 = maxSpeed * smoothTime;
                num4 = Clamp(num4, -num6, num6);
                target = current - num4;
                float num7 = (currentVelocity + num * num4) * deltaTime;
                currentVelocity = (currentVelocity - num * num7) * num3;
                float num8 = target + (num4 + num7) * num3;
                if (num5 - current > 0f == num8 > num5) {
                        num8 = num5;
                        currentVelocity = (num8 - num5) / deltaTime;
                }
                return num8;
        }

        public static float Clamp(float value, float min, float max) {
                if (value < min) {
                        value = min;
                } else {
                        if (value > max) {
                                value = max;
                        }
                }
                return value;
        }

        public static float Max(float a, float b) {
                return (a <= b) ? b : a;
        }
        Random random = new Random();

        public float Range(float Min, float Max) {
                double range = Max - Min;
                double scaled = random.nextDouble() * range;
                double shifted = scaled + Min;
                return (float) shifted;
                //return Min + (float) (Math.random() * ((Max - Min) + 1));


        }
        
        
         public void setUpParticles(AssetManager assetManager) {
               Material mat = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
                mat.setTexture("Texture", assetManager.loadTexture("Effects/Explosion/spark.png"));

                spark1.setStartColor(new ColorRGBA(1f, 0.8f, 0.36f, 1));
                spark1.setEndColor(new ColorRGBA(1f, 0.8f, 0.36f, 0f));
                spark1.setStartSize(.5f);
                spark1.setEndSize(.5f);
                spark1.setFacingVelocity(true);
                spark1.setParticlesPerSec(0);
                spark1.setGravity(0, 2, 0);
                spark1.setLowLife(0);
                spark1.setHighLife(.05f);
                spark1.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 20, 0));
                spark1.getParticleInfluencer().setVelocityVariation(1);
                spark1.setImagesX(1);
                spark1.setImagesY(1);
                // Material mat = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
                //mat.setTexture("Texture", assetManager.loadTexture("Effects/Explosion/spark.png"));
                spark1.setMaterial(mat);
                // rootNode.attachChild(spark1);

                smoke.setStartColor(new ColorRGBA(.1f, .1f, .1f, 1));
                smoke.setStartSize(.1f);
                smoke.setEndSize(2f);
                smoke.setFacingVelocity(false);
                smoke.setParticlesPerSec(0);
                smoke.setGravity(0, 0, 0);
                smoke.setLowLife(.1f);
                smoke.setHighLife(5f);
                smoke.getParticleInfluencer().setInitialVelocity(new Vector3f(0, .5f, 0));
                smoke.getParticleInfluencer().setVelocityVariation(.25f);
                smoke.setImagesX(2);
                smoke.setImagesY(2);
                smoke.setInWorldSpace(true);
                smoke.setRandomAngle(true);
                smoke.setSelectRandomImage(true);

                Material mat2 = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
                mat2.setTexture("Texture", assetManager.loadTexture("Effects/Explosion/flame.png"));
                //mat2.setTexture("Texture", assetManager.loadTexture("Textures/SmokePuff.png"));
                //mat2.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);

                smoke.setMaterial(mat2);
                //  rootNode.attachChild(smoke);
                flash.setStartColor(new ColorRGBA(1f, 0.8f, 0.36f, 1));
                flash.setEndColor(new ColorRGBA(1f, 0.8f, 0.36f, 0f));
                flash.setStartSize(.7f);
                flash.setEndSize(.35f);
                flash.setFacingVelocity(false);
                flash.setParticlesPerSec(0);
                flash.setGravity(0, 0, 0);
                flash.setLowLife(.05f);
                flash.setHighLife(.1f);
                flash.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 0, 0));
                //flash.getParticleInfluencer().setVelocityVariation(1);
                flash.setImagesX(2);
                flash.setImagesY(2);
                flash.setInWorldSpace(false);
                flash.setSelectRandomImage(true);
                Material mat1 = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
                mat1.setTexture("Texture", assetManager.loadTexture("Effects/Explosion/flash.png"));

                flash.setMaterial(mat1);



        }
}
