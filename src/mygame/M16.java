/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.asset.TextureKey;
import com.jme3.audio.AudioNode;
import com.jme3.material.Material;
import com.jme3.math.Quaternion;
import com.jme3.math.Transform;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.texture.Texture;
import com.jme3.texture.TextureCubeMap;

/**
 *
 * @author James
 */
public class M16 extends Weapon {

    public Spatial model;

    public M16(AssetManager assetManager, String sight, String filename, String name) {
        //super(assetManager, (Node) assetManager.loadModel("Models/M16Hands/M16.j3o"));
        super(assetManager, (Node) assetManager.loadModel("Models/Guns/M16A2.j3o"), filename, sight, name);
        
        
        //super(assetManager, (Node) assetManager.loadModel("Models/Guns/M16A2.j3o"), filename, sight, name);
      //  name = "M16";
        //set Vars....
        HipLoc = new Vector3f(-0.319f, -1.005f, 1.69f);
        IronSightsAimLoc = new Vector3f(-0.005f, -1.003f, 1.18f);
        zoomAmount = 35;
        AcogzoomAmount = 20;
        //currentZoom = 20;
        if ("ACOG".equals(sight)){
              currentZoom = AcogzoomAmount;  
        }
        if ("IRON".equals(sight)){
              currentZoom = zoomAmount;  
        }
        muzzle = (Node) weapon.getChild("muzzle");
        ROF = 14;
        numBulletsinClip = 30;
        TotalBullets = 120;
        
        clipSize = 30;
        currentAmmo = clipSize;
        sightSpeed = 20.0F;
        Recoilz = 1.5f;
        recoilspeed = 3;
        recoilback = 1;
        sound = new AudioNode(assetManager, "Sounds/M16sound.wav");
        sound.setPositional(false);
        sound.setVolume(.5f);
        weapon.attachChild(sound);
        sound.setLocalTranslation(muzzle.getLocalTranslation());
        burstNum = 3;
        orig = new Vector3f(0,0,0);
        gunHolder.attachChild(RecoilHolder);
        //RecoilHolder.attachChild(weapon);
        ejectionPort = (Node) weapon.getChild("Ejection");
        
        weapon.setLocalTranslation(0,0,0);
        runQuat = new Quaternion(0.069f, 0.1649f, 0.0f, 1.0f);
        runLoc = new Vector3f(0.289f, -1.189f, 1.53f);
        camRecoilRotation = new Vector3f(0, 0, 0.01f);
        camRecoilSpeed = 5.0f;
        RecoilKickback = new Vector3f(0.025f, 0.025f, -.2f);
        AimRecoilKickback = new Vector3f(.008f, .008f, -.05f);
        RecoilRotation = new Vector3f(0, .01f, 0);
        AimRecoilRotation = new Vector3f(0, .005f, 0);
        AcogSightsAimLoc = new Vector3f(-0.005f, -0.958f, 1.18f);
        sightType = sight;

 }

    @Override
    public void update(float tpf) {
       // System.out.println(" Loc " + aimLoc);
        super.update(tpf);
        
       
        
        
    }

    

}
