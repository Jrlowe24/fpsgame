/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.collision.shapes.CompoundCollisionShape;
import com.jme3.bullet.control.VehicleControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

/**
 *
 * @author James Lowe
 */
public class Car extends VehicleControl implements ActionListener {
    
    boolean using = false;
    private final float accelerationForce = 1000.0f;
    private final float deccelerationForce = 2000.0f;
    private final float brakeForce = 400.0f;
    private float steeringValue = 0;
    private float accelerationValue = 0;
    private Vector3f jumpForce = new Vector3f(0, 3000, 0);
    // private VehicleControl vehicle;
    private Node car;
    private Node wheelmodelR;
    private Node wheelmodelL;
    private float Mph;
   // private boolean player = false;
    public static final Quaternion YAW180 = new Quaternion().fromAngleAxis(FastMath.PI, new Vector3f(0, 1, 0));

    public Car(Node rootNode, AssetManager assetManager, BulletAppState bulletAppState, Vector3f loc, InputManager inputManager) {

        car = (Node) assetManager.loadModel("Models/Buggy5/Buggy5scene.j3o");
        // car.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);
        CompoundCollisionShape ccs = new CompoundCollisionShape();
        CollisionShape carmesh = CollisionShapeFactory.createDynamicMeshShape(car.getChild("Collision"));
        System.out.println("Children" + car.getChildren());
        ccs.addChildShape(carmesh, Vector3f.ZERO);
        //this.setEnabled(false);
        this.setMass(800);
        this.setCollisionShape(carmesh);
        car.addControl(this);
        rootNode.attachChild(car);
        bulletAppState.getPhysicsSpace().add(this);
        this.setPhysicsLocation(loc);
        addWheels(assetManager);
       // if (isplayer) {
            setUpKeysCar(inputManager);
        //}



    }

    public void addWheels(AssetManager assetManager) {
        float stiffness = 10.0f;//200=f1 car
        float compValue = .1f; //(should be lower than damp)
        float dampValue = .3f;
        this.setSuspensionCompression(compValue * 2.0f * FastMath.sqrt(stiffness));
        this.setSuspensionDamping(dampValue * 2.0f * FastMath.sqrt(stiffness));
        this.setSuspensionStiffness(stiffness);
        this.setMaxSuspensionForce(10000.0f);


        Vector3f wheelDirection = new Vector3f(0, -1, 0);
        Vector3f wheelAxle = new Vector3f(-1, 0, 0);
        float frontRadius = 1.2f;
        float rearRadius = 1.7f;
        float restLength = 0.6f;
        float yOff = .8f;
        float xOff = 1.3f;
        float zOff = 2.4f;
        //Node node1 = new Node("wheel 1 node");
        //node1.setLocalTranslation(car.getChild("FL").getWorldTranslation());
        //Geometry wheels1 = new Geometry("wheel 1", wheelMesh);
        Spatial wheels1 = car.getChild("FL");
        //node1.attachChild(wheels1);
        //wheels1.setMaterial(wheel);
        //wheels1.rotate(0, FastMath.HALF_PI, 0);
        this.addWheel(wheels1, wheels1.getWorldTranslation(),
                wheelDirection, wheelAxle, restLength, frontRadius, true);



        //Node node1 = new Node("wheel 1 node");
        //node1.setLocalTranslation(car.getChild("FL").getWorldTranslation());
        //Geometry wheels1 = new Geometry("wheel 1", wheelMesh);
        Spatial wheels2 = car.getChild("FR");
        //node1.attachChild(wheels1);
        //wheels1.setMaterial(wheel);
        //wheels1.rotate(0, FastMath.HALF_PI, 0);
        this.addWheel(wheels2, wheels2.getWorldTranslation(),
                wheelDirection, wheelAxle, restLength, frontRadius, true);


        //Node node1 = new Node("wheel 1 node");
        //node1.setLocalTranslation(car.getChild("FL").getWorldTranslation());
        //Geometry wheels1 = new Geometry("wheel 1", wheelMesh);
        Spatial wheels3 = car.getChild("BR");
        //node1.attachChild(wheels1);
        //wheels1.setMaterial(wheel);
        //wheels1.rotate(0, FastMath.HALF_PI, 0);
        this.addWheel(wheels3, wheels3.getWorldTranslation(),
                wheelDirection, wheelAxle, restLength, rearRadius, false);



        //Node node1 = new Node("wheel 1 node");
        //node1.setLocalTranslation(car.getChild("FL").getWorldTranslation());
        //Geometry wheels1 = new Geometry("wheel 1", wheelMesh);
        Spatial wheels4 = car.getChild("BL");
        //node1.attachChild(wheels1);
        //wheels1.setMaterial(wheel);
        //wheels1.rotate(0, FastMath.HALF_PI, 0);
        this.addWheel(wheels4, wheels4.getWorldTranslation(),
                wheelDirection, wheelAxle, restLength, rearRadius, false);


        //car.attachChild(node1);
        //car.attachChild(node2);
        //car.attachChild(node3);
        // car.attachChild(node4);
        this.getWheel(0).setFrictionSlip(10f);
        this.getWheel(1).setFrictionSlip(10f);
        this.getWheel(2).setFrictionSlip(9f);
        this.getWheel(3).setFrictionSlip(9f);
        // vehicle.getWheel(2).set
        //vehicle.getWheel(3).setFrictionSlip(1.5f);
        //vehicle.getWheel(0).setRollInfluence(.5f);
        //vehicle.getWheel(1).setRollInfluence(.5f);
        //vehicle.getWheel(2).setRollInfluence(.01f);
        //vehicle.getWheel(3).setRollInfluence(.01f);



    }

    private void setUpKeysCar(InputManager inputManager) {
        inputManager.addMapping("Lefts", new KeyTrigger(KeyInput.KEY_F));
        inputManager.addMapping("Rights", new KeyTrigger(KeyInput.KEY_H));
        inputManager.addMapping("Ups", new KeyTrigger(KeyInput.KEY_T));
        inputManager.addMapping("Downs", new KeyTrigger(KeyInput.KEY_G));
        inputManager.addMapping("Space", new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addMapping("Reset", new KeyTrigger(KeyInput.KEY_RETURN));
        inputManager.addListener(this, "Lefts");
        inputManager.addListener(this, "Rights");
        inputManager.addListener(this, "Ups");
        inputManager.addListener(this, "Downs");
        inputManager.addListener(this, "Space");
        inputManager.addListener(this, "Reset");
    }

    public VehicleControl getVehicle() {
        return this;
    }

    public Node getCar() {
        return car;
    }

    public float getSpeed() {
        return (this.getCurrentVehicleSpeedKmHour() * .62f);
    }

    public void onAction(String binding, boolean value, float tpf) {
        if (using) {
            if (binding.equals("Lefts")) {
                if (value) {
                    steeringValue += .5f;
                } else {
                    steeringValue += -.5f;
                }
                this.steer(steeringValue);
            } else if (binding.equals("Rights")) {
                if (value) {
                    steeringValue += -.5f;
                } else {
                    steeringValue += .5f;
                }
                this.steer(steeringValue);
            } else if (binding.equals("Ups")) {
                if (value) {
                    accelerationValue += accelerationForce;
                } else {
                    accelerationValue -= accelerationForce;
                }
                this.accelerate(accelerationValue);
            } else if (binding.equals("Downs")) {
                if (value) {

                    accelerationValue -= accelerationForce;

                } else {

                    accelerationValue += accelerationForce;
                }
                this.accelerate(accelerationValue);
            } else if (binding.equals("Space")) {
                if (value) {
                    this.brake(brakeForce);
                } else {
                    this.brake(0f);
                }


            } else if (binding.equals("Reset")) {
                if (value) {
                    System.out.println("Reset");
                    this.setPhysicsLocation(new Vector3f(0, -3, 0));
                    this.setPhysicsRotation(YAW180);
                    this.setLinearVelocity(Vector3f.ZERO);
                    this.setAngularVelocity(Vector3f.ZERO);
                    this.resetSuspension();
                } else {
                }
            }
        }
    }

    public void setPlayer(boolean isplayer) {
        //isplayer = player;
    }

    public void simpleUpdate() {
    }
}
