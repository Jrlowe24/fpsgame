/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Quad;

/**
 *
 * @author James
 */
public class Bullet_Hole {

    public Bullet_Hole(Vector3f loc, Vector3f norm, Node hitObj, Material material, Node rootNode) {
        Quad quad = new Quad(.15f, .15f);
        Geometry geom = new Geometry("bullet hole", quad);
        geom.setMaterial(material);
        Vector3f temp = new Vector3f();
        temp.set(norm);
        temp.normalizeLocal();

        temp.multLocal(.01f);
        temp.addLocal(loc);
        hitObj.worldToLocal(temp, temp);
        
        geom.setLocalTranslation(temp);
        geom.setQueueBucket(RenderQueue.Bucket.Transparent);
        hitObj.attachChild(geom);
        //rootNode.attachChild(geom);
        temp.set(norm);
        temp.addLocal(loc);
        geom.lookAt(temp, Vector3f.UNIT_Y);
        //geom.move(-.075f, 0, -.075f);
    }
}
