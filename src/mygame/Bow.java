/*
 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

/**
 *
 * @author James
 */
public class Bow extends Weapon {

    public Spatial model;

    public Bow(AssetManager assetManager, String name) {
            
        super(assetManager, (Node) assetManager.loadModel("Models/Bow/BowAndArrow.j3o"), null, null, name);
       // weapon = (Node) assetManager.loadModel("Models/ak47_1/ak47_6.j3o");
        //weapon = (Node) assetManager.loadModel("Models/Guns/AK47.j3o");
        //set Vars....
       // name = "AK47";
        HipLoc = new Vector3f(-0.38f, -1f, 1.74f);
        IronSightsAimLoc = new Vector3f(-0.001f, -0.95f, 1.27f);
         zoomAmount = 35;
        currentZoom = 35;
        //AcogzoomAmount = 25;
//        SwayLeft = new Quaternion(0.0f, 0.0f, 0.04f, 0);
//        SwayRight = new Quaternion(0.0f, 0.0f, -0.04f, 0);
//        Orig = weapon.getLocalRotation();
//        SwaySpeed = 3F;
        muzzle = (Node) weapon.getChild("muzzle");
        ROF = 10;
        numBulletsinClip = 1;
        TotalBullets = 10;
        clipSize = 15;
        currentAmmo = clipSize;
        sightSpeed = 20.0F;
        Recoilz = 4f;
        recoilspeed = 3;
        recoilback = 1;
        //sound = new AudioNode(assetManager, "Sounds/M16sound.wav");
        sound.setPositional(false);
        sound.setVolume(.5f);
        weapon.attachChild(sound);
        sound.setLocalTranslation(muzzle.getLocalTranslation());
        burstNum = 1;
        orig = new Vector3f(0,0,0);
        gunHolder.attachChild(RecoilHolder);
        RecoilHolder.attachChild(weapon);
        //Bolt = (Node) weapon.getChild("Bolt");
        //boltSpeed = 5.0f;
        //ejectionPort = (Node) weapon.getChild("Ejection");
        weapon.setLocalTranslation(0,0,0);
        runQuat = new Quaternion(0.069f, 0.1649f, 0.0f, 1.0f);
        runLoc = new Vector3f(0.289f, -1.189f, 1.53f);
        camRecoilRotation = new Vector3f(0, 0, 0.01f);
        camRecoilSpeed = 5.0f;
        RecoilKickback = new Vector3f(0.035f, 0.035f, -.25f);
        AimRecoilKickback = new Vector3f(.01f, .01f, -.05f);
        RecoilRotation = new Vector3f(0, .01f, 0);
        AimRecoilRotation = new Vector3f(0, .005f, 0);
        sightType = "IRON";
 }

    @Override
    public void update(float tpf) {
        //System.out.println(" Loc " + HipLoc + " Scale " + weapon.getLocalScale());
        super.update(tpf);
        
       
        
        
    }

    

}

