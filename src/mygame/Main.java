package mygame;

//import com.jme3.ai.navmesh.NavMesh;
import com.jme3.app.SimpleApplication;
import com.jme3.audio.AudioNode;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.collision.shapes.CompoundCollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.light.PointLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.post.FilterPostProcessor;
import com.jme3.post.HDRRenderer;
import com.jme3.post.filters.BloomFilter;
import com.jme3.post.filters.DepthOfFieldFilter;
import com.jme3.post.filters.FogFilter;
import com.jme3.post.filters.LightScatteringFilter;
import com.jme3.post.filters.RadialBlurFilter;
import com.jme3.post.ssao.SSAOFilter;
import com.jme3.scene.CameraNode;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.shadow.DirectionalLightShadowFilter;
import com.jme3.shadow.EdgeFilteringMode;
import com.jme3.ui.Picture;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import java.util.ArrayList;

/**
 * FPS Engine V.3
 *
 * @author James
 */
public class Main extends SimpleApplication implements ActionListener, ScreenController {

    private Vector3f walkDirection = new Vector3f();
    private boolean Crouch = false, jump = false, left = false, right = false, up = false, down = false, running = false, Shoot = false, ADS = false, turn = false, Grenade = false, F = false, B = false, L = false, R = false, U = false, C = false, o = false, p = false, pickUp = false;
    private CharacterControl player;
    int scale = 1;
    private BulletAppState bulletAppState;
    private RigidBodyControl MapMesh;
    private Node Map;
    private CameraNode PlayerChar;
    private float sightSpeed = 20f;
    private float FOV = 0F;
    private float defaultFOV = 45.0F;
    Node muz;
    AudioNode M16shot;
    AudioNode HitMark;
    float aspect;
    private FilterPostProcessor fpp;
    DepthOfFieldFilter dofFilter;
    float delay = 0;
    float ROF = 20;
    public float swayspeed = 3f;
    public String Weapon = "M162";
    public boolean Hasgun = true;
    public M16 m16;
    public float distanceCheckdelay;
    public float walkspeed = .55F;
    public float adsspeed = .2F;
    ParticleEmitter spark1 = new ParticleEmitter("spark", ParticleMesh.Type.Triangle, 10);
    ParticleEmitter flash = new ParticleEmitter("spark", ParticleMesh.Type.Triangle, 2);
    ParticleEmitter smoke = new ParticleEmitter("spark", ParticleMesh.Type.Triangle, 10);
    ParticleEmitter spark = new ParticleEmitter("spark", ParticleMesh.Type.Triangle, 20);
    ParticleEmitter dirt = new ParticleEmitter("spark", ParticleMesh.Type.Triangle, 50);
    ParticleEmitter wood = new ParticleEmitter("spark", ParticleMesh.Type.Triangle, 30);
    public float jumpRotSpeed = 6f;
    public float jumpRetSpeed = 10f;
    float gunRotationSpeed = .1f;
    public Quaternion lastCamDir = new Quaternion();
    Picture CrossHairs;
    Picture hitmarker;
    public boolean ShowCrossHairs = false;
    public float CrossHairTime;
    float accuracy;
    public Node Flash;
    public Node bulletSpawn;
    public ArrayList<Enemy> AI;
    public ArrayList<CoverPoint> coverPoint;
    public ArrayList<WanderPoint> wanderPoint;
    public ArrayList<Weapon> weapons;
    public ArrayList<Node> SpawnPoint;
    public Geometry playerGeo;
    public Node playerModel;
    boolean Driving = false;
    Car currentCar;
    Weapon currentWeapon;
    MiniMap mm;
    Material bullethole;
    Weapon primary;
    Weapon secondary;
    Weapon M16reg;
    Weapon ACW;
    Vector3f Cross1orig;
    Vector3f Cross1move;
    ParticleEmitter ejection = new ParticleEmitter("Ejection", ParticleMesh.Type.Triangle, 3);
    CapsuleCollisionShape crouch;
    CapsuleCollisionShape stand;
    Bullet_Hole Hole;
    public float bulletPower = 50;
    ParticleEmitter smoke2 = new ParticleEmitter("Smoke", ParticleMesh.Type.Triangle, 30);
    float CamShake = 15f;
    float shootTime = 1;
    ParticleEmitter tracer = new ParticleEmitter("Tracer", ParticleMesh.Type.Triangle, 2);
    float headbobStepCounter;
    float headbobSpeed = 9.0f;
    float headbobAmountX = .001f;
    float headbobAmountY = .001f;
    float currentheadbobX;
    float currentheadbobY;
    float followSpeed = 7;
    String CarView = "driver";
//    NavMesh navMesh;

    public static void main(String[] args) {
        Main app = new Main();

        app.start();
        // AppSettings newSetting = new AppSettings(true);
        // newSetting.setFrameRate(60);
        //  app.setSettings(newSetting); 
        app.settings.setFrameRate(70);
        //app.settings.setFrequency(60);
        app.restart();
    }
    private Nifty nifty;

    @Override
    public void simpleInitApp() {
        //Gui();
        StartGame();


    }

    public void StartGame() {
        bulletAppState = new BulletAppState();
        stateManager.attach(bulletAppState);
        setUpPlayer();
        setUpMap();
        setUpKeys();
        setUpGun();
        M16shot = new AudioNode(assetManager, "Sounds/M16sound.wav");
        HitMark = new AudioNode(assetManager, "Sounds/HitMark.wav");
        HitMark.setPositional(false);
        HitMark.setDirectional(false);
        HitMark.setVolume(1);
        aspect = cam.getWidth() / cam.getHeight();
        for (int e = 0; e < 5; e++) {
            //CreateEnemy();
        }
        mm = new MiniMap(cam, renderManager, rootNode, player, guiNode, assetManager);

    }

    private void Gui() {
        inputManager.setCursorVisible(true);
        flyCam.setEnabled(false);
        NiftyJmeDisplay niftyDisplay = new NiftyJmeDisplay(assetManager,
                inputManager,
                audioRenderer,
                guiViewPort);

        nifty = niftyDisplay.getNifty();

        //nifty.fromXml("Interface/newNiftyGui.xml", "start", this);
        nifty.fromXml("Interface/StartButton.xml", "start", this);
        //nifty.fromXml("Interface/newdropdown.xml", "start", this);
        //nifty.fromXml("Interface/newscrollbar.xml", "start", this);
        nifty.addScreen("Play", new Screen(nifty, "Play", this, null));


        guiViewPort.addProcessor(niftyDisplay);
        StartGame();
    }

    public void startGame(String nextScreen) {
        nifty.gotoScreen(nextScreen);  // switch to another screen
        // start the game and do some more stuff...
    }

    private void setUpPlayer() {
        PlayerChar = new CameraNode("gun", cam);
        crouch = new CapsuleCollisionShape(2f, 3);
        stand = new CapsuleCollisionShape(2f, 6);
        player = new CharacterControl(stand, 1.5f);
        player.setPhysicsLocation(new Vector3f(0, 5, -10));
        player.setJumpSpeed(15);
        player.setMaxSlope(.5f);
        bulletAppState.getPhysicsSpace().add(player);
        //Box playerbox = new Box(.5f, capsule.getHeight(), .5f);
        //playerGeo = new Geometry("Player", playerbox);
        //playerGeo.setLocalTranslation(0, 0, 0);
        //Material mat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        bulletAppState.getPhysicsSpace().enableDebug(assetManager);

        playerModel = (Node) assetManager.loadModel("Models/Soldier/Soldier_1.j3o");
        playerModel.setUserData("Player", "Player");
        rootNode.attachChild(playerModel);
        //playerGeo.setMaterial(mat);
        //rootNode.attachChild(playerGeo);


        //int numofenemies = 3;
        //for (int i = 1; i < numofenemies; i++) {
        // Enemy AItest = new Enemy(assetManager, bulletAppState, navMesh);
        // AItest.Soldier.setLocalTranslation(50, 0, -50);

        // AI.add(AItest);



    }

    private void setUpGun() {

        // hole = 
        bulletSpawn = new Node("bulletSpawn");

        bulletSpawn.setLocalTranslation(new Vector3f(cam.getLocation().x, cam.getLocation().y, cam.getLocation().z + .7f));
        weapons = new ArrayList<Weapon>();

        primary = new M16(assetManager, "IRON", "M16IronSight", "M16 W/ IRON");
        primary.setInputManager(inputManager);
        primary.addPhysicsWeapon(bulletAppState);
        currentWeapon = primary;
        weapons.add(primary);


        secondary = new AK74(assetManager, "AK 47");
        secondary.setInputManager(inputManager);
        //rootNode.attachChild(secondary.getParticleEmitter());
        secondary.addPhysicsWeapon(bulletAppState);
        weapons.add(secondary);
        
        // primary = new Bow(assetManager, "Bow");
       // primary.setInputManager(inputManager);
       // primary.addPhysicsWeapon(bulletAppState);
      //  currentWeapon = primary;
      //  weapons.add(primary);



        // M16reg = new M16(assetManager, "IRON", "M16IronSight", "M16");
        // M16reg.setInputManager(inputManager);
        //M16reg.weapon.detachChild(M16reg.Hands);
        //rootNode.attachChild(M16reg.weapon);
        //M16reg.addPhysicsWeapon(bulletAppState);
        //M16reg.weaponControl.setEnabled(true);
        //M16reg.weaponControl.setPhysicsLocation(new Vector3f(10, 3, 0));
        //weapons.add(M16reg);

        // ACW = new ACW(assetManager, "IRON", "ACWIronSights", "ACW W/ IronSights");
        // ACW.setInputManager(inputManager);
        // ACW.weapon.detachChild(ACW.Hands);
        // rootNode.attachChild(ACW.weapon);
        // ACW.addPhysicsWeapon(bulletAppState);
        // ACW.weaponControl.setEnabled(true);
        // ACW.weaponControl.setPhysicsLocation(new Vector3f(5, 5, -3));
        // weapons.add(ACW);

        // Weapon M4 = new M4A1(assetManager, "IRON", "M4A1IronSight", "M4A1 w/ Iron");
        //M4.setInputManager(inputManager);
        //M4.addPhysicsWeapon(bulletAppState);

        //currentWeapon = M4;
        //primary = currentWeapon;
        // weapons.add(M4);



//                Weapon G36 = new G36(assetManager, "IRON", "G36IronSight", "G36 w/ Iron");
//                G36.setInputManager(inputManager);
//                //rootNode.attachChild(M4.getParticleEmitter());
//                G36.weapon.detachChild(G36.Hands);
//                rootNode.attachChild(G36.weapon);
//                G36.addPhysicsWeapon(bulletAppState);
//                G36.weaponControl.setEnabled(true);
//                G36.weaponControl.setPhysicsLocation(new Vector3f(5, 3, 10));
//                weapons.add(G36);



        setHoldingWeapon(currentWeapon);

        PlayerChar.attachChild(bulletSpawn);
        rootNode.attachChild(PlayerChar);
        hitmarker = new Picture("HitMarker");
        hitmarker.setImage(assetManager, "Textures/HitMarker.png", true);
        hitmarker.setWidth(20);
        hitmarker.setHeight(20);
        hitmarker.setPosition(cam.getWidth() / 2 - 10, cam.getHeight() / 2 - 5);

        CrossHairs = new Picture("CrossHairs");
        CrossHairs.setImage(assetManager, "Textures/CrossHairs_1.png", true);
        CrossHairs.setWidth(50);
        CrossHairs.setHeight(50);
        CrossHairs.setPosition(cam.getWidth() / 2 - 24, cam.getHeight() / 2 - 20);
        guiNode.attachChild(CrossHairs);
        // System.out.println(" CH loc " + CrossHairs.getLocalTranslation());
        bullethole = assetManager.loadMaterial("Materials/BulletholeMat.j3m");



        for (Weapon w : weapons) {
            rootNode.attachChild(w.flash);
            rootNode.attachChild(w.smoke);
            rootNode.attachChild(w.spark1);
            rootNode.attachChild(w.ejection);

        }
        // rootNode.attachChild(spark);
        //rootNode.attachChild(spark1);

        setUpParticles();


    }

    public void setHoldingWeapon(Weapon weapon) { // gets a primary or secondary
        if (currentWeapon != weapon) { // if a different weapon..
            currentWeapon.RecoilHolder.detachChild(currentWeapon.weapon);//hides current gun
            currentWeapon = weapon;
            PlayerChar.attachChild(currentWeapon.gunHolder);
            currentWeapon.RecoilHolder.attachChild(currentWeapon.weapon);
            //weapon.weapon.setLocalTranslation(currentWeapon.RecoilHolder.getLocalTranslation());

            //  weapon.weapon.setL
        } else {
            PlayerChar.attachChild(currentWeapon.gunHolder);
            currentWeapon.RecoilHolder.attachChild(currentWeapon.weapon);
        }
        //currentWeapon = primary;
    }

    public void setNewWeapon(Weapon NewGun) {
        NewGun.weaponControl.setEnabled(false); // disables new gun 
        ThrowGun(currentWeapon);

        if (currentWeapon == primary) {  //if the gun you were holding was a primary:
            primary = NewGun;      // set the primary to the new gun
            currentWeapon = primary;
            rootNode.detachChild(primary.weapon);
            PlayerChar.attachChild(primary.gunHolder);
            primary.RecoilHolder.attachChild(primary.weapon);
            primary.weapon.attachChild(primary.Hands);
            primary.weapon.setLocalTranslation(primary.HipLoc);
            primary.gunHolder.setLocalTranslation(primary.HipLoc);

        }
        if (currentWeapon == secondary) {  //if the gun you were holding was a secondary:
            secondary = NewGun;      // set the secondary to the new gun
            currentWeapon = secondary;
            rootNode.detachChild(secondary.weapon);
            PlayerChar.attachChild(secondary.gunHolder);
            secondary.RecoilHolder.attachChild(secondary.weapon);
            secondary.weapon.attachChild(secondary.Hands);
            secondary.weapon.setLocalTranslation(secondary.HipLoc);
            secondary.gunHolder.setLocalTranslation(secondary.HipLoc);
        }

    }

    public void ThrowGun(Weapon old) { // detaches from player and propels forwards
        PlayerChar.detachChild(currentWeapon.gunHolder);
        rootNode.attachChild(currentWeapon.weapon);
        currentWeapon.weaponControl.setEnabled(true);
        currentWeapon.weaponControl.setLinearVelocity(cam.getDirection().mult(3f));
        currentWeapon.weapon.detachChild(currentWeapon.Hands);

    }
    
        private Picture dispQuad;
          HDRRenderer hdrRender;

    private void setUpMap() {


        bulletAppState.getPhysicsSpace().setGravity(new Vector3f(0, -10, 0));
        //Map = (Node) assetManager.loadModel("Scenes/TestMap.j3o");
        Map = (Node) assetManager.loadModel("Scenes/Map.j3o");

        CompoundCollisionShape cs = new CompoundCollisionShape();
        com.jme3.bullet.collision.shapes.CollisionShape terrainmesh = CollisionShapeFactory.createMeshShape(Map.getChild("Collision"));
        MapMesh = new RigidBodyControl(terrainmesh, 0);
        rootNode.attachChild(Map);
        bulletAppState.getPhysicsSpace().add(MapMesh);



        DirectionalLight sun = new DirectionalLight();
        sun.setDirection((new Vector3f(.8f, -1, .5f)).normalizeLocal());
        sun.setColor(ColorRGBA.White);
        rootNode.addLight(sun);
        
        AmbientLight ambient = new AmbientLight();
        ambient.setColor(ColorRGBA.White);
        
       
        rootNode.addLight(ambient);
        
        FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
        LightScatteringFilter sunlight = new LightScatteringFilter(new Vector3f(.8f, -1, .5f).mult(-3000));
         //fpp.addFilter(sunlight);
        sunlight.setNbSamples(40);
        sunlight.setLightDensity(.2f);
        fpp.setNumSamples(1);

        dofFilter = new DepthOfFieldFilter();
      // dofFilter.setFocusDistance(5);
       // dofFilter.setFocusRange(1);
        dofFilter.setBlurScale(.8f);
       // fpp.addFilter(dofFilter);
        //viewPort.addProcessor(fpp);
        
       
        
       
        
       // HDRRenderer hdr = new HDRRenderer(assetManager, renderer);
       //// hdr.setExposure(1);
        
       // fpp.addFilter(hdr);


        FogFilter fog = new FogFilter();
        fog.setFogColor(new ColorRGBA(0.9f, 0.9f, 0.9f, 1.0f));
        fog.setFogDistance(1500);
        fog.setFogDensity(.8f);
        //fpp.addFilter(fog);
        //viewPort.addProcessor(fpp);
        RadialBlurFilter rbf = new RadialBlurFilter();
        rbf.setSampleDistance(1);
        rbf.setSampleStrength(1);
        //fpp.addFilter(rbf);



        /* this shadow needs a directional light */
       // DirectionalLightShadowFilter dlsr = new DirectionalLightShadowFilter(assetManager, 1024, 3);
       //  dlsr.setEnabledStabilization(true);
       //  dlsr.setLight(sun);
       //  dlsr.setEdgeFilteringMode(EdgeFilteringMode.Bilinear);
       //  fpp.addFilter(dlsr);
         //viewPort.addProcessor(dlsr);

       // PssmShadowRenderer pssm = new PssmShadowRenderer(assetManager, 1024, 3);
       // pssm.setDirection(new Vector3f(.8f, -1, .5f).normalizeLocal());
       // ViewPort.addProcessor(pssm);
          DirectionalLightShadowFilter dlsf = new DirectionalLightShadowFilter(assetManager, 1024, 3);
         dlsf.setLight(sun);
         fpp.addFilter(dlsf);
        dlsf.setEnabledStabilization(true);
         dlsf.setEdgeFilteringMode(EdgeFilteringMode.PCFPOISSON);
         
   //       PointLight PL = new PointLight();
     //   PL.setPosition(Map.getChild("LightTest").getWorldTranslation());
       // rootNode.addLight(PL);

          BloomFilter bloom = new BloomFilter(BloomFilter.GlowMode.Scene);
        bloom.setDownSamplingFactor(2);
        bloom.setBlurScale(2f);
        bloom.setExposurePower(5f);
        bloom.setExposureCutOff(0.5f);
        bloom.setBloomIntensity(.8f);
       // fpp.addFilter(bloom);
        
        viewPort.addProcessor(fpp);

        coverPoint = new ArrayList<CoverPoint>();
        wanderPoint = new ArrayList<WanderPoint>();
        SpawnPoint = new ArrayList<Node>();
        Material mat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");

        int numcp = 30;
        int numwp = 5;
        int numsp = 3;
        for (int i = 0; i < numcp; i++) {
          //  CoverPoint cptemp = new CoverPoint(Map.getChild("CoverPoint" + i).getWorldTranslation(), rootNode, mat);
           // coverPoint.add(cptemp);
        }
        for (int i = 0; i < numwp; i++) {
          //  WanderPoint wp = new WanderPoint(Map.getChild("WanderPoint" + i).getWorldTranslation(), rootNode, mat);
          //  wanderPoint.add(wp);
        }
        for (int i = 0; i < numsp; i++) {
            Node a = (Node) Map.getChild("Spawn" + i);
            System.out.println(a);
            SpawnPoint.add(a);
        }
        System.out.println(wanderPoint);


        //Spatial mapnav = assetManager.loadModel("Scenes/MapNavMesh1.j3o");
        Geometry mapnav = (Geometry) Map.getChild("NavMesh");
        Mesh mesh = mapnav.getMesh();
        //Mesh mesh = findGeom(mapnav, "NavMesh").getMesh();
//        navMesh = new NavMesh(mesh);




        AI = new ArrayList<Enemy>();
        int numofenemies = 3;
        //for (int i = 1; i < numofenemies; i++) {
        //Enemy AItest = new Enemy(assetManager, bulletAppState, navMesh);
        //AItest.Soldier.setLocalTranslation(50, 0, -50);
        //rootNode.attachChild(AItest);
         //Enemy AItest1 = new Enemy(rootNode, assetManager, bulletAppState, navMesh);
        // AItest1.setLocalTranslation(-50, 0, 20);
        //  Enemy AItest2 = new Enemy(rootNode, assetManager, bulletAppState, navMesh);
        //AItest2.setLocalTranslation(15, 0, 8);

         //AI.add(AItest);
        //AI.add(AItest1);
        //AI.add(AItest2);
        for (Enemy e : AI) {
            rootNode.attachChild(e);
              //e.setPoints(coverPoint, wanderPoint);
        }


        //   Car buggy = new Car(rootNode, assetManager, bulletAppState, new Vector3f(-29, 5, -43), inputManager);
        // currentCar = buggy;
        //flyCam.setMoveSpeed(30);
        //CreateEnemy();



    }

    private Geometry findGeom(Spatial spatial, String name) {
        if (spatial instanceof Node) {
            Node node = (Node) spatial;
            for (int i = 0; i < node.getQuantity(); i++) {
                Spatial child = node.getChild(i);
                Geometry result = findGeom(child, name);
                if (result != null) {
                    return result;
                }
            }
        } else if (spatial instanceof Geometry) {
            if (spatial.getName().startsWith(name)) {
                return (Geometry) spatial;
            }
        }
        return null;
    }

    private void setUpKeys() {
        inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_S));
        inputManager.addMapping("Jump", new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addMapping("Sprint", new KeyTrigger(KeyInput.KEY_LSHIFT));
        inputManager.addMapping("Shoot", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
        inputManager.addMapping("ADS", new MouseButtonTrigger(MouseInput.AXIS_Y));
        inputManager.addMapping("B", new KeyTrigger(KeyInput.KEY_B));
        inputManager.addMapping("F", new KeyTrigger(KeyInput.KEY_F));
        inputManager.addMapping("L", new KeyTrigger(KeyInput.KEY_L));
        inputManager.addMapping("R", new KeyTrigger(KeyInput.KEY_R));
        inputManager.addMapping("U", new KeyTrigger(KeyInput.KEY_U));
        inputManager.addMapping("C", new KeyTrigger(KeyInput.KEY_C));
        inputManager.addMapping("o", new KeyTrigger(KeyInput.KEY_O));
        inputManager.addMapping("p", new KeyTrigger(KeyInput.KEY_P));
        inputManager.addMapping("Primary", new KeyTrigger(KeyInput.KEY_1));
        inputManager.addMapping("Secondary", new KeyTrigger(KeyInput.KEY_2));
        inputManager.addMapping("pickUp", new KeyTrigger(KeyInput.KEY_E));




        inputManager.addListener(this, "Left");
        inputManager.addListener(this, "Right");
        inputManager.addListener(this, "Up");
        inputManager.addListener(this, "Down");
        inputManager.addListener(this, "Jump");
        inputManager.addListener(this, "Shoot");
        inputManager.addListener(this, "Sprint");
        inputManager.addListener(this, "ADS");
        inputManager.addListener(this, "L");
        inputManager.addListener(this, "B");
        inputManager.addListener(this, "R");
        inputManager.addListener(this, "F");
        inputManager.addListener(this, "U");
        inputManager.addListener(this, "C");
        inputManager.addListener(this, "o");
        inputManager.addListener(this, "p");
        inputManager.addListener(this, "Primary");
        inputManager.addListener(this, "Secondary");
        inputManager.addListener(this, "pickUp");
    }

    public void onAction(String binding, boolean value, float tpf) {
        if (binding.equals("Left")) {
            left = value;
        } else if (binding.equals("Right")) {
            right = value;
        } else if (binding.equals("Up")) {
            up = value;
        } else if (binding.equals("Down")) {
            down = value;
        } else if (binding.equals("Jump")) {
            jump = true;
            player.jump();
        } else if (binding.equals("Sprint")) {
            running = value;
        } else if (binding.equals("Shoot")) {
            Shoot = value;
        } else if (binding.equals("ADS")) {
            ADS = value;
        } else if (binding.equals("L")) {
            L = value;

        } else if (binding.equals("R")) {
            R = value;
        } else if (binding.equals("F")) {
            F = value;
        } else if (binding.equals("B")) {
            B = value;
        } else if (binding.equals("U")) {
            U = value;
        } else if (binding.equals("C")) {
            C = value;
        } else if (binding.equals("o")) {
            o = value;
        } else if (binding.equals("p")) {
            p = value;
        } else if (binding.equals("Primary")) {
            setHoldingWeapon(primary);
        } else if (binding.equals("Secondary")) {
            setHoldingWeapon(secondary);
        } else if (binding.equals("pickUp") && !value) {

            pickUp = true;

        }
    }

    @Override
    public void simpleUpdate(float tpf) {
        
        distanceCheckdelay++;
        if (currentWeapon != null) {
            recoil(tpf);
            currentWeapon.update(tpf);
        }
        //System.out.println("X rotation" + cam.getDirection().x);
        // System.out.println(" Primary " + primary.name);
        //  System.out.println(" Secondary " + secondary.name);
        // System.out.println(pickUp);
        //m16.update(tpf);
        //  System.out.println("ACWLOc " + ACW.weapon.getWorldTranslation());


        if (mm != null) {
            mm.Update(tpf);
        }
        if (AI != null) {
            for (Enemy e : AI) {
                e.setPlayerLoc(player.getPhysicsLocation());
                e.Update(tpf);
            }

            if (delay > 30) {
                //       e.findNextPoint();
                //       delay = 0;
            }

        }
        //AItest.Update(tpf);
        if (coverPoint != null) {
            for (CoverPoint c : coverPoint) {
                c.Update(tpf);
                //c.setPlayerLoc(new Vector3f(PlayerChar.getLocalTranslation().x, PlayerChar.getLocalTranslation().y, PlayerChar.getLocalTranslation().z));
//            c.setPlayerLoc(new Vector3f(playerGeo.getLocalTranslation().x, playerGeo.getLocalTranslation().y, playerGeo.getLocalTranslation().z));
                c.setPlayerLoc(playerModel.getWorldTranslation());
            }
        }

        //for (BulletHole b : bulletholes) {
        //    if (b.LifeTime > 0) {
        //        b.Update(tpf);
        //    }
        //      if (b.LifeTime <= 0) {
        //          //bulletholes.remove(b);
        //         //rootNode.detachChild(b);
        //     }



        //System.out.println(" Cam Rot " + cam.getRotation());
        listener.setLocation(cam.getLocation());
        if (L) {
            //        currentWeapon.weapon.getLocalTranslation().setX(currentWeapon.weapon.getLocalTranslation().x + .005f);
            //currentWeapon.HipLoc.setX(currentWeapon.HipLoc.x + .005f);
            currentWeapon.runLoc.setX(currentWeapon.runLoc.x - .001f);
        } else if (B) {
            // currentWeapon.Bolt.getLocalTranslation().setX( currentWeapon.Bolt.getLocalTranslation().z + .005f);
            //         currentWeapon.weapon.getLocalTranslation().setZ(currentWeapon.weapon.getLocalTranslation().z - .005f);
        } else if (F) {

            //        currentWeapon.weapon.getLocalTranslation().setZ(currentWeapon.weapon.getLocalTranslation().z + .005f);
            currentWeapon.runLoc.setZ(currentWeapon.runLoc.z + .001f);
        } else if (R) {
            //currentWeapon.weapon.getLocalRotation().set(currentWeapon.weapon.getLocalRotation().getX(), currentWeapon.weapon.getLocalRotation().getY() + .005f, currentWeapon.weapon.getLocalRotation().getZ(), currentWeapon.weapon.getLocalRotation().getW());
            //   currentWeapon.HipLoc.setX(currentWeapon.HipLoc.x - .005f);
            currentWeapon.runLoc.setX(currentWeapon.runLoc.x + .001f);
        } else if (U) {
            // setSight("ACOG", "ACOG");
            //  currentWeapon.weapon.getLocalRotation().set(currentWeapon.weapon.getLocalRotation().getX() + .005f, currentWeapon.weapon.getLocalRotation().getY(), currentWeapon.weapon.getLocalRotation().getZ(), currentWeapon.weapon.getLocalRotation().getW());
        } else if (C) {
            currentWeapon.runLoc.setY(currentWeapon.runLoc.y - .001f);
            // setSight("M16IronSight", "IRON");
            //currentWeapon.weapon.getLocalTranslation().setY(currentWeapon.weapon.getLocalTranslation().y - .005f);
            // for (Enemy a : AI) {
            //     a.findNextPoint();
            // }
            //currentWeapon.HipLoc.setY(currentWeapon.HipLoc.y - .005f);
        } else if (o) {
            System.out.println(rootNode.getChildren());
            //  currentWeapon.weapon.getLocalScale().addLocal(.01f, .01f, .01f);
            // Vector3f = player.getPhysicsLocation().distance()
        } else if (p) {
            currentWeapon.weapon.getLocalScale().addLocal(-.01f, -.01f, -.01f);
        }
        // delay += timer.getTimePerFrame() * 20;
        if (player != null) {
            movement(tpf);
        }

        if (C) {
            player.setCollisionShape(crouch);
        } else {
            //player.setCollisionShape(stand);
        }

        float distance = 99999f;
        Weapon closest = null;
        if (weapons != null) {
            for (Weapon w : weapons) {
                if (w != primary && w != secondary) {
                    float Distance = PlayerChar.getWorldTranslation().distance(w.weapon.getWorldTranslation());

                    //  System.out.println(w.name + " is " + Distance + " away from me ");
//
                    if (Distance < distance) {
                        distance = Distance;
                        closest = w;

                    }
                }


            }
        }

        if (distance < 30) {
            // System.out.println("PickUp " + closest.name + "?");
            if (pickUp) {
                pickUp = false;
                setNewWeapon(closest);

            }
        }




        //System.out.println("RunLoc" + currentWeapon.runLoc);
        //System.out.println("HipLoc" + currentWeapon.HipLoc);
        //System.out.println(" Aim Loc " + currentWeapon.IronSightsAimLoc);
        //System.out.println(" WorldScale " + currentWeapon.weapon.getWorldScale());
    }

    public void CreateEnemy() {
//        Enemy AItest = new Enemy(assetManager, bulletAppState, navMesh);
//        AItest.setPoints(coverPoint, wanderPoint, SpawnPoint);
//        System.out.println(wanderPoint);
//        int sp = Range(0, SpawnPoint.size());
//        AItest.Soldier.setLocalTranslation(SpawnPoint.get(sp).getWorldTranslation());
//        AI.add(AItest);
//        rootNode.attachChild(AItest);
//        AItest.setNewDestination();

//        Enemy AItest = new Enemy(assetManager, bulletAppState, navMesh);
     //   AItest.setPoints(coverPoint, wanderPoint, SpawnPoint);


    //    AI.add(AItest);
    //    rootNode.attachChild(AItest);
   //     AItest.setNewDestination();

    }

    public void camFollow(float tpf) {
        if ("trail".equals(CarView)) {
            PlayerChar.getWorldTranslation().interpolate(currentCar.getCar().getChild("Trail").getWorldTranslation(), tpf * followSpeed);
            PlayerChar.lookAt(currentCar.getCar().getWorldTranslation(), Vector3f.UNIT_Y);
        }
        if ("driver".equals(CarView)) {
            PlayerChar.setLocalTranslation(currentCar.getCar().getChild("Driver").getWorldTranslation());

            //cam.lookAt(currentCar.getCar().getWorldTranslation(), Vector3f.UNIT_Y);
        }
    }

    public void movement(float tpf) {
        if (Driving) {
            PlayerChar.detachChild(currentWeapon.weapon);
            player.setEnabled(false);
            //rootNode.detachChild(currentWeapon.weapon);
            camFollow(tpf);
        }
        if (!Driving) {
            //                  headbobStepCounter += tpf * headbobSpeed;
            //                  currentheadbobX = (float) Math.sin(headbobStepCounter) * headbobAmountX;
            //                  currentheadbobY = (float) Math.cos(headbobStepCounter * 2) * -headbobAmountY;
            //                  cam.getLocation().set(cam.getLocation().x + currentheadbobX, cam.getLocation().y + currentheadbobY, cam.getLocation().z);

            CrossHairTime += tpf;
            if (ShowCrossHairs && CrossHairTime < .1f) {
                guiNode.attachChild(hitmarker);

            }
            if (CrossHairTime > .1f) {
                guiNode.detachChild(hitmarker);
            }

            PlayerChar.setLocalRotation(cam.getRotation());

            Vector3f camDir = cam.getDirection().clone().multLocal(walkspeed);
            Vector3f camLeft = cam.getLeft().clone().multLocal(walkspeed - .05F);
            walkDirection.set(0, 0, 0);
            if (left) {
                walkDirection.addLocal(camLeft);

            }
            if (right) {
                walkDirection.addLocal(camLeft.negate());
            }
            if (up) {
                walkDirection.addLocal(camDir.getX(), 0, camDir.getZ());

            }

            if (down) {
                walkDirection.addLocal(camDir.negate().getX(), 0, camDir.negate().getZ());

            }
            if (Crouch) {
            }
            if (running && up && !ADS) {
                walkspeed = .6f;
            } else if (!ADS) {
                walkspeed = .35f;
            } else if (ADS) {
                walkspeed = .2f;

            }

            player.setWalkDirection(walkDirection);

            if (player.onGround()) {
                jump = false;
            } else {
                jump = true;
            }

            PlayerChar.setLocalTranslation(new Vector3f( //set Cam to player
                    player.getPhysicsLocation().getX(),
                    player.getPhysicsLocation().getY() + 1, //to be 6 units tall
                    player.getPhysicsLocation().getZ()));

            playerModel.setLocalTranslation(new Vector3f( //
                    player.getPhysicsLocation().getX(),
                    player.getPhysicsLocation().getY() - 4f,
                    player.getPhysicsLocation().getZ()));

            ADS();
            distanceCheckdelay += tpf * 20;
        }
    }

    public void shoot() {
        CollisionResults results = new CollisionResults();
        Ray bullet = new Ray(bulletSpawn.getWorldTranslation(), cam.getDirection().add(((float) Math.random() * accuracy) - ((float) Math.random() * accuracy), ((float) Math.random() * accuracy) - ((float) Math.random() * accuracy), 0));

        for (Spatial s : rootNode.getChildren()) {
            if (!(s instanceof ParticleEmitter)) {
                s.collideWith(bullet, results);
                //System.out.println("spatials " + s.getParent().getParent().getParent().getParent());
            }
        }
        //        System.out.println(rootNode.getChildren());
        if (results.size() > 0) { //if hit something
            CollisionResult impact = results.getClosestCollision(); //first impact
            Enemy enemyObj = (Enemy) impact.getGeometry().getUserData("EnemyObject");
            String material = impact.getGeometry().getUserData("Type");
            makeParticles(material, impact);
            //System.out.println(" Object " + impact.getGeometry().getUserData("Type"));
            // System.out.println(enemyObj);
            if (impact.getGeometry().getUserData("Strength") != null) {
                if (enemyObj == null) {
                    //System.out.println(" Checking Penetration ");
                    //float strength = impact.getGeometry().getUserData("Strength");
                    //impact = CheckPenetration(impact, bullet, strength);
                    //System.out.println(" Strength " + strength);
                }
            }
            //System.out.println(" Target " + impact.getGeometry().getName() + " strength " + strength);
            //impact = CheckPenetration(impact, bullet); //returns new impact target
            //enemyObj = (Enemy) impact.getGeometry().getUserData("EnemyObject");
            // enemyObj = (Enemy) impact.getGeometry().getParent().getParent().getParent().getParent().getParent().getUserData("EnemyObject");
            enemyObj = (Enemy) impact.getGeometry().getUserData("EnemyObject");
            //impact = CheckPenetration(impact, bullet);
            if (enemyObj != null) {
                //System.out.println(" My Location: " + cam.getLocation() + "Enemy Location: " + enemyObj.getWorldTranslation() + "Camera Direction: " + cam.getDirection());
                HitMark.playInstance();
                ShowCrossHairs = true;
                CrossHairTime = 0;
                enemyObj.DecrementHealth(25);
                if (enemyObj.shot(impact.getGeometry(), bulletPower)) {   // shot returns true when enemy is dead
                    //rootNode.detachChild(enemyObj);
                    // AI.remove(enemyObj);
                    //   CreateEnemy();
                }
            }
        }
        results.clear();
    }

    public void recoil(float tpf) {
        if (currentWeapon.currentAmmo > 0) {
            if (Shoot && !running) {

                if (shootTime <= 0) { //if can shoot
                    shoot();

                    shootTime = 1;

                }
            }
            shootTime -= tpf * currentWeapon.ROF;
        }
    }

    public void ADS() {
        if (ADS) {
            accuracy = .01f;
            guiNode.detachChild(CrossHairs);
              if (distanceCheckdelay > 20) {
            CollisionResults results1 = new CollisionResults();
            Ray distancetest = new Ray(currentWeapon.muzzle.getWorldTranslation(), cam.getDirection());
            Map.collideWith(distancetest, results1);
            for (Spatial s : rootNode.getChildren()) {
                if (!(s instanceof ParticleEmitter)) {
                       s.collideWith(distancetest, results1);
                }
            }

              if (results1.size() > 0) { //if hit something
               CollisionResult impact = results1.getClosestCollision();
               float distance = impact.getDistance();
                       // System.out.println("Focusdistance " + dofFilter.getFocusDistance());
                          System.out.println("Focusdistance " + distance);
               dofFilter.setFocusDistance(distance / 9.4F);
               //dofFilter.setFocusDistance(distance);
               dofFilter.setFocusRange(3);
               results1.clear();
             }
             results1.clear();
            distanceCheckdelay = 0;
             }
            dofFilter.setEnabled(true);

            if (FOV != currentWeapon.currentZoom) {
                FOV = lerp(FOV, currentWeapon.currentZoom, (timer.getTimePerFrame() <= 1.0F ? timer.getTimePerFrame() : 1.0F) * sightSpeed);
            } else if (FOV - currentWeapon.currentZoom < 1f) {
                FOV = currentWeapon.currentZoom;
            }
            cam.setFrustumPerspective(FOV, 1280F / 720F, .05f, 350);




        } else {
            accuracy = .08f;
            guiNode.attachChild(CrossHairs);
            dofFilter.setEnabled(false);
            if (FOV != defaultFOV) {
                FOV = lerp(FOV, defaultFOV, (timer.getTimePerFrame() <= 1.0F ? timer.getTimePerFrame() : 1.0F) * sightSpeed);
            } else if (FOV - defaultFOV < 1f) {
                FOV = defaultFOV;
            }
            cam.setFrustumPerspective(FOV, 1280F / 720F, .05f, 350);
           

        }
    }

    public void makeSparks(Vector3f Loc, Vector3f norm) {
        //ParticleEmitter spark = new ParticleEmitter("spark", ParticleMesh.Type.Triangle, 15);
        spark.killAllParticles();
        spark.setFaceNormal(norm);
        spark.getParticleInfluencer().setInitialVelocity(norm.multLocal(10, 10, 10));
        // rootNode.attachChild(spark);
        spark.setLocalTranslation(Loc);
        spark.emitAllParticles();



    }

    public void makeDirt(Vector3f Loc) {

        dirt.setStartColor(new ColorRGBA(1f, 0.8f, 0.36f, 1));
        dirt.setEndColor(new ColorRGBA(1f, 0.8f, 0.36f, 1));
        dirt.setStartSize(.5f);
        dirt.setEndSize(.5f);
        dirt.setFacingVelocity(false);
        dirt.setParticlesPerSec(0);
        dirt.setGravity(0, 2, 0);
        dirt.setLowLife(0);
        dirt.setHighLife(.4f);
        dirt.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 15, 0));
        dirt.getParticleInfluencer().setVelocityVariation(1);
        dirt.setImagesX(1);
        dirt.setImagesY(1);
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
        mat.setTexture("Texture", assetManager.loadTexture("Effects/Explosion/spark.png"));
        dirt.setMaterial(mat);
        rootNode.attachChild(dirt);
        dirt.setLocalTranslation(Loc);
        dirt.emitAllParticles();

    }

    public void makeWood(Vector3f Loc, Vector3f norm) {

        wood.setStartColor(new ColorRGBA(1, 1, 1, 1));
        wood.setEndColor(new ColorRGBA(1, 1, 1f, 1f));
        wood.setStartSize(.5f);
        wood.setEndSize(.2f);
        wood.setFacingVelocity(false);
        wood.setParticlesPerSec(0);
        wood.setGravity(0, 10, 0);
        wood.setLowLife(0);
        wood.setHighLife(.5f);
        wood.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 5, 0));
        wood.setFaceNormal(norm);
        wood.getParticleInfluencer().setVelocityVariation(.4f);
        wood.setImagesX(4);
        wood.setImagesY(4);
        wood.setRandomAngle(true);
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
        mat.setTexture("Texture", assetManager.loadTexture("Textures/WoodChips.png"));
        wood.setMaterial(mat);
        rootNode.attachChild(wood);
        wood.setLocalTranslation(Loc);
        wood.emitAllParticles();
    }

    public void makeBlood(Vector3f Loc) {
    }

    public void makeSmoke(Vector3f Loc, Vector3f norm) {
        smoke2.setStartColor(new ColorRGBA(0, 0, 0, .5f));
        smoke2.setEndColor(new ColorRGBA(.1f, .1f, .1f, 1f));
        smoke2.setStartSize(.4f);
        smoke2.setEndSize(1.4f);
        smoke2.setFacingVelocity(false);
        smoke2.setParticlesPerSec(0);
        smoke2.setGravity(0, 4, 0);
        smoke2.setLowLife(.05f);
        smoke2.setHighLife(.5f);
        smoke2.setFaceNormal(norm);
        smoke2.getParticleInfluencer().setInitialVelocity(norm.mult(5));
        smoke2.getParticleInfluencer().setVelocityVariation(.05f);
        smoke2.setImagesX(1);
        smoke2.setImagesY(15);
        smoke2.setInWorldSpace(true);
        smoke2.setRandomAngle(true);
        smoke2.setSelectRandomImage(true);
        Material mat2 = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
        // mat2.setTexture("Texture", assetManager.loadTexture("Textures/SmokePuff.png"));
        smoke2.setMaterial(mat2);
        // rootNode.attachChild(smoke2);
        smoke2.setLocalTranslation(Loc);
        //smoke2.emitAllParticles();

    }

    public float lerp(float a, float b, float percentage) {
        return a * (1 - percentage) + b * percentage;
    }

    public void setUpParticles() {
        spark.setStartColor(new ColorRGBA(1f, 0.65f, 0f, 1));
        spark.setEndColor(new ColorRGBA(1f, 0.65f, 0f, .35f));
        spark.setStartSize(.6f);
        spark.setEndSize(.6f);
        spark.setFacingVelocity(true);
        spark.getParticleInfluencer().setInitialVelocity(new Vector3f(10, 10, 10));
        spark.setParticlesPerSec(0);
        spark.setGravity(0, 15, 0);
        spark.setLowLife(.05f);
        spark.setHighLife(.3f);

        spark.getParticleInfluencer().setVelocityVariation(.6f);
        spark.setImagesX(1);
        spark.setImagesY(1);
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
        mat.setTexture("Texture", assetManager.loadTexture("Effects/Explosion/spark.png"));
        spark.setMaterial(mat);
        rootNode.attachChild(spark);
    }

    private CollisionResult CheckPenetration(CollisionResult impact, Ray bullet, float Strength) {

        float Penetration = bulletPower / (Strength * 10);
//        System.out.println(" Penetration = " + Penetration);
        Ray bullet2 = new Ray(impact.getContactPoint().add(bullet.getDirection().mult(Penetration)), bullet.getDirection()); //new ray from impact
        CollisionResults results2 = new CollisionResults();


        for (Spatial s : rootNode.getChildren()) {
            if (!(s instanceof ParticleEmitter)) {
                //  if (!(s instanceof BulletHole)) {
//
                s.collideWith(bullet2, results2); //all spatials collide with bullet 2
                // }
            }
        }
        CollisionResult impact2 = results2.getClosestCollision(); // new impact results

        if (results2.size() > 0) {
            if (impact2.getGeometry() != impact.getGeometry()) {
                System.out.println("New Object = " + impact.getGeometry().getName());
                return impact2;

            } else {
                System.out.println("Same Object");
                return impact;


            }

        } else {
            System.out.println("Same Object");
            return impact;
        }


    }

    private void makeParticles(String material, CollisionResult impact) {
        if ("Metal".equals(material)) {

            //BulletHole b = new BulletHole(assetManager, rootNode, impact.getContactPoint(), impact.getContactNormal());
            makeSparks(impact.getContactPoint(), impact.getContactNormal());

            Hole = new Bullet_Hole(impact.getContactPoint(), impact.getContactNormal(), impact.getGeometry().getParent(), bullethole, rootNode);



//            bulletholes.add(b);
        }
        if ("Dirt".equals(material)) {
            //BulletHole b = new BulletHole(assetManager, rootNode, impact.getContactPoint(), impact.getContactNormal());
            makeDirt(impact.getContactPoint());

            //bulletholes.add(b);
        }
        if ("Wood".equals(material)) {

            //BulletHole b = new BulletHole(assetManager, rootNode, impact.getContactPoint(), impact.getContactNormal());
            makeWood(impact.getContactPoint(), impact.getContactNormal());
            //bulletholes.add(b);
        }
        if ("Flesh".equals(material)) {
            makeBlood(impact.getContactPoint());
        }

        if ("Stone".equals(material)) {
            makeSmoke(impact.getContactPoint(), impact.getContactNormal());
            Hole = new Bullet_Hole(impact.getContactPoint(), impact.getContactNormal(), impact.getGeometry().getParent(), bullethole, rootNode);

            // System.out.println("SMOKE");
        }
    }

    public static float SmoothDamp(float current, float target, float currentVelocity, float smoothTime, float maxSpeed, float deltaTime) {
        smoothTime = Max(0.0001f, smoothTime);
        float num = 2f / smoothTime;
        float num2 = num * deltaTime;
        float num3 = 1f / (1f + num2 + 0.48f * num2 * num2 + 0.235f * num2 * num2 * num2);
        float num4 = current - target;
        float num5 = target;
        float num6 = maxSpeed * smoothTime;
        num4 = Clamp(num4, -num6, num6);
        target = current - num4;
        float num7 = (currentVelocity + num * num4) * deltaTime;
        currentVelocity = (currentVelocity - num * num7) * num3;
        float num8 = target + (num4 + num7) * num3;
        if (num5 - current > 0f == num8 > num5) {
            num8 = num5;
            currentVelocity = (num8 - num5) / deltaTime;
        }
        return num8;
    } //smootly moves a number to another number

    public static float Clamp(float value, float min, float max) {
        if (value < min) {
            value = min;
        } else {
            if (value > max) {
                value = max;
            }
        }
        return value;
    } //clamps value in a rang

    public static float Max(float a, float b) {
        return (a <= b) ? b : a;
    } //returns max number

    public int Range(int Min, int Max) {
        return Min + (int) (Math.random() * ((Max - Min)));
    } // returns in within range

    @Override
    public void bind(Nifty nifty, Screen screen) {
        System.out.println("bind( " + screen.getScreenId() + ")");
    }

    @Override
    public void onStartScreen() {
    }

    @Override
    public void onEndScreen() {
        StartGame();
    }
}
