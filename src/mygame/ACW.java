/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.math.Quaternion;
import com.jme3.math.Transform;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

/**
 *
 * @author James
 */
public class ACW extends Weapon {

    public Spatial model;

    public ACW(AssetManager assetManager, String sight, String filename, String name) {
        //super(assetManager, (Node) assetManager.loadModel("Models/M16Hands/M16.j3o"));
       // super(assetManager, (Node) assetManager.loadModel("Models/Guns/M16A2.j3o"), filename, sight);
          
        super(assetManager, (Node) assetManager.loadModel("Models/Guns/ACW.j3o"), filename, sight, name);
      //  name = "M16";
        //set Vars....
        HipLoc = new Vector3f(-0.376f, -1.086f, 1.69f);
        IronSightsAimLoc = new Vector3f(-0.06f, -1.084f, 1.18f);
        zoomAmount = 35;
        AcogzoomAmount = 20;
        //currentZoom = 20;
        if ("ACOG".equals(sight)){
              currentZoom = AcogzoomAmount;  
        }
        if ("IRON".equals(sight)){
              currentZoom = zoomAmount;  
        }
        muzzle = (Node) weapon.getChild("muzzle");
        ROF = 12;
        numBulletsinClip = 30;
        TotalBullets = 80;
        
        clipSize = 15;
        currentAmmo = clipSize;
        sightSpeed = 20.0F;
        Recoilz = 1.5f;
        recoilspeed = 3;
        recoilback = 1;
        sound = new AudioNode(assetManager, "Sounds/M16sound.wav");
        sound.setPositional(false);
        sound.setVolume(.5f);
        weapon.attachChild(sound);
        sound.setLocalTranslation(muzzle.getLocalTranslation());
        burstNum = 1;
        orig = new Vector3f(0,0,0);
        gunHolder.attachChild(RecoilHolder);
         Bolt = (Node) weapon.getChild("BOLT");
        //RecoilHolder.attachChild(weapon);
        ejectionPort = (Node) weapon.getChild("Ejection");
        
        weapon.setLocalTranslation(0,0,0);
        runQuat = new Quaternion(0.069f, 0.1649f, 0.0f, 1.0f);
        runLoc = new Vector3f(0.289f, -1.351f, 1.53f);
        camRecoilRotation = new Vector3f(0, 0, 0.01f);
        camRecoilSpeed = 5.0f;
        RecoilKickback = new Vector3f(0.028f, 0.028f, -.22f);
        AimRecoilKickback = new Vector3f(.01f, .01f, -.08f);
        RecoilRotation = new Vector3f(0, .012f, 0);
        AimRecoilRotation = new Vector3f(0, .005f, 0);
        AcogSightsAimLoc = new Vector3f(-0.005f, -0.958f, 1.18f);
        sightType = sight;

 }

    @Override
    public void update(float tpf) {
       // System.out.println(" Loc " + aimLoc);
        super.update(tpf);
        
       
        
        
    }

    

}
