/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.collision.CollisionResults;
import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Sphere;

/**
 *
 * @author James
 */
public class WanderPoint extends Node {

    Node rootNode;
    CollisionResults results = new CollisionResults();
    Vector3f eyeHeight = new Vector3f(0, 7, 0);
    Vector3f kneeHeight = new Vector3f(0, 3, 0);
    Sphere s = new Sphere(16, 16, 1f);
    Geometry sphere = new Geometry("GEo", s);

    public WanderPoint(Vector3f loc, Node rootNode, Material test) {
        setLocalTranslation(loc.x, 0, loc.z);
        this.attachChild(sphere);
        sphere.setMaterial(test);
        sphere.setLocalTranslation(this.getWorldTranslation());
        this.rootNode = rootNode;
        rootNode.attachChild(sphere);

    }

    public void Update(float tpf) {
    }
}
