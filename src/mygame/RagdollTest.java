/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.control.KinematicRagdollControl;
import com.jme3.scene.Node;

/**
 *
 * @author James
 */
public class RagdollTest {
        
        public KinematicRagdollControl ragdoll;
        public Node model;
        
        public RagdollTest(AssetManager assetManager){
                ragdoll = new KinematicRagdollControl(.5f);
                model = (Node) assetManager.loadModel("Models/RagDoll/SkeletonTest.j3o");
                model.addControl(ragdoll);
                
        }
        
        
        
}
