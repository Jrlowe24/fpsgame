/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

//import com.jme3.ai.navmesh.NavMesh;
//import com.jme3.ai.navmesh.NavMeshPathfinder;
//import com.jme3.ai.navmesh.Path;
import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.AnimEventListener;
import com.jme3.animation.LoopMode;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.bounding.BoundingVolume;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.KinematicRagdollControl;
import com.jme3.collision.CollisionResults;
import com.jme3.effect.ParticleEmitter;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.SceneGraphVisitor;
import com.jme3.scene.Spatial;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Random;

/**
 *
 * @author James
 */
public class Enemy extends Node implements AnimEventListener {

    public AssetManager assetManager;
    public int Health;
    Geometry Body;
    Geometry Head;
    //public Spatial enemy;
    Node Soldier;
    BulletAppState bulletAppState;
    KinematicRagdollControl ragdoll;
    public final float headDamage = 80;
    public final float BodyDamage = 35;
    public final float LegDamage = 15;
    public final float HelmetDamage = 10;
//    public NavMeshPathfinder navi;
    public ArrayList<CoverPoint> coverPoint;
    public ArrayList<WanderPoint> WanderPoint;
    public float FollowMovementSpeed = 10;
    public boolean move = true;
    public boolean canSeeEnemy = true;
    public String STATE = null;
    public Vector3f playerLoc;
    public Spatial player;
    boolean canSeePlayer;
    float removeCountDown = 20;
    Node Ai = new Node();
    float MovementSpeed = 9;
    boolean covering = true;
    boolean running = true;
    public Node muzzle;
    AudioNode gunShot;
    float reactDelay = 30;
    AnimChannel channel;
    AnimControl a;
    float gunShootWait;
    public Vector3f lastPlayerLoc = new Vector3f();
    boolean atDestination;
    CollisionResults hit;
    float shootTime;
    float ROF = 10;
    boolean pausedShooting;
    boolean pausedShootingCounting;
    boolean chasing;
    ArrayList<CoverPoint> LOSpoints = new ArrayList<CoverPoint>();
    ArrayList<CoverPoint> LOSCoverpoints = new ArrayList<CoverPoint>();
    ArrayList<Node> SpawnPoint = new ArrayList<Node>();
    CoverPoint currentpoint;
    Vector3f lastUsedPlayerLoc = new Vector3f();

    public Enemy(AssetManager assetManager, BulletAppState bulletAppState){// NavMesh navMesh) {
        this.assetManager = assetManager;
        this.bulletAppState = bulletAppState;
    //    if (navMesh != null) {
            //navi = new NavMeshPathfinder(navMesh);
   //     }
        gunShot = new AudioNode(assetManager, "Sounds/M16sound.wav");

        //navi.setEntityRadius(3);
        Health = 100;
        //makeShape();
        createSoldier();
        setUpSightControl();
        //findNextPoint();
        STATE = "Patrolling";
        
    }

    public void createSoldier() {

           //Soldier = (Node) assetManager.loadModel("Models/RagDoll/RagdollTest.j3o");
        //Soldier = (Node) assetManager.loadModel("Models/Soldier/Soldier.j3o");
        //Soldier = (Node) assetManager.loadModel("Models/Soldier/SoldierW/6.mesh.xml");
        //Soldier = (Node) assetManager.loadModel("Models/Soldier/SoldierW/6.mesh.j3o");
        // Soldier = (Node) assetManager.loadModel("Models/Sinbad/Sinbad.mesh.xml");

        Soldier = (Node) assetManager.loadModel("Models/Soldier/BFSoldier11.j3o");
        muzzle = (Node) Soldier.getChild("muzzle");
        Soldier.setUserData("Type", "Flesh");
        Soldier.setUserData("EnemyObject", this);
//        Soldier.getChild("Head").setUserData("Type", "Flesh");
//        Soldier.getChild("Helmet").setUserData("Type", "Metal");
        //      Soldier.getChild("Body").setUserData("Type", "Flesh");
//        Soldier.getChild("Legs").setUserData("Type", "Flesh");
//        Soldier.getChild("Head").setUserData("EnemyObject", this);
//        Soldier.getChild("Helmet").setUserData("EnemyObject", this);
//        Soldier.getChild("Helmet").setUserData("EnemyObject", this);
        Soldier.getChild("Body.001").setUserData("EnemyObject", this);
        Soldier.getChild("Head").setUserData("EnemyObject", this);
        Soldier.getChild("BackPack").setUserData("EnemyObject", this);
        Soldier.getChild("Medic").setUserData("EnemyObject", this);
        Soldier.getChild("UsKit").setUserData("EnemyObject", this);
//        Soldier.getChild("Legs").setUserData("EnemyObject", this);
//        Soldier.getChild("Head").setUserData("Strength", new Float(5));
//        Soldier.getChild("Helmet").setUserData("Strength", new Float(8));
//        Soldier.getChild("Body").setUserData("Strength", new Float(5));
//        Soldier.getChild("Legs").setUserData("Strength", new Float(5));



          //a = Soldier.getChild("SkeletonControl").getControl(AnimControl.class);
          //a.addListener(this);
          //channel = a.createChannel();

         ragdoll = new KinematicRagdollControl(.5f);


         Soldier.addControl(ragdoll);
         bulletAppState.getPhysicsSpace().add(ragdoll);
        //setupSinbad(ragdoll);
        //setUpBones();

        //a = Soldier.getChild("Anim").getControl(AnimControl.class);
       // a.addListener(this);
        //channel = a.createChannel();
        //channel.setAnim("Action.001");
        //channel.setLoopMode(LoopMode.Loop);
        //channel.setSpeed(1);

        this.attachChild(Soldier);
        Ai.setLocalTranslation(Soldier.getWorldTranslation());
        






    }

    public boolean shot(Geometry g, float power) {

        if ("Head".equals(g.getName())) {
            DecrementHealth(80f);
            // System.out.println("shot head");
        } else if ("Body".equals(g.getName())) {
            DecrementHealth(30f);
            // System.out.println("Shot body");
        } else if ("Legs".equals(g.getName())) {
            DecrementHealth(10f);
            // System.out.println("shot Legs");
        } else if ("Helmet".equals(g.getName())) {
            DecrementHealth(8f);
            //   System.out.println("shot body");
        }


        System.out.println(" Part " + g.getName());
        return Health <= 0 ? true : false;
    }

    public void DecrementHealth(float value) {
        Health -= value;
        // System.out.println("EnemyHealth = " + Health);
        if (Health <= 0) {
            die();
                 ragdoll.setEnabled(true);
                ragdoll.setRagdollMode();
        } else {
        }
    }

    public void Update(float tpf) {
        reactDelay -= 10 * tpf;

        if (Health <= 0) {
            //        removeCountDown -= tpf * 10;
            //  this.detachChild(Soldier);
        }
        if (removeCountDown <= 0) {
            //   ragdoll.setEnabled(true);
            //   ragdoll.setRagdollMode();
            // this.detachChild(Soldier);
        }
        Animations();


        if (canSeePlayer) {
            chasing = false;
            lastPlayerLoc.set(playerLoc);
            lastPlayerLoc.y = 0;
            //    System.out.println("CanSeePlayer");
            //Soldier.lookAt(new Vector3f(playerLoc.x, 0, playerLoc.z), Vector3f.UNIT_Y);
            //shoot(tpf);

        }

       SeeEnemy();

        if (reactDelay < 0 && Health > 0) {
            //findNextPoint();
            reactDelay = 0;
        }

//        if (navi != null && Health > 0) {

            //moveToDestination(tpf);

  //      }

  //      if (!navi.isAtGoalWaypoint()) {
    //        if ("Patrolling".equals(STATE)) {
  //              Patrol();
//            }
  //      }
  //      if (navi.isAtGoalWaypoint()){
            if ("Patrolling".equals(STATE)){
                //navi.clearPath();
              //  Patrol();
            }
     //   }

    }

    public void Patrol() {
        Random ran = new Random();
        int x = ran.nextInt(WanderPoint.size());
        //int x = 4;

        // if (oldWanderPoint != x) { //if the new Point is different that the last one

        //    oldWanderPoint = x;
     //   navi.setPosition(Soldier.getWorldTranslation());

        if (WanderPoint.get(x) != null) {
    //        navi.computePath(WanderPoint.get(x).getWorldTranslation());
        }


        // if (oldWanderPoint == x) { //if the new Point is the same as the last one, find a new one
        //   while (x == oldWanderPoint) {
        ///       x = ran.nextInt(3);
        //   }

        //   if (WanderPoint.get(x) != null) {
        //       navi.setPosition(Soldier.getWorldTranslation());
        //       navi.computePath(WanderPoint.get(x).getWorldTranslation());
        ///   }

        //}
    }

    //  public enum State
    // {
    ////     Patrol, //wanders
    //      Investigate, //follows clues from player (sound, sight)
    //     Cover //takes closests cover
    //  }
    public void shoot(float tpf) {


        if (!pausedShooting) {
            // if (gunShootWait){
            if (shootTime <= 0) {
                shootTime = 1;
                gunShot.setLocalTranslation(muzzle.getWorldTranslation());
                gunShot.setPositional(false);
                gunShot.playInstance();

                Vector3f DirectionToPlayer = playerLoc.subtract(Soldier.getWorldTranslation()).normalize();
//       Ray bullet = new Ray(Soldier.getWorldTranslation(), DirectionToPlayer);
//        
//            hit = new CollisionResults();
//
//            Node rootNode = this.getParent();
//            Node collide = new Node();
//            for (Spatial s : rootNode.getChildren()) {
//                //System.out.println(s.getName());
//                if (!(s instanceof Enemy)) {
//                    if (!(s instanceof ParticleEmitter)) {
//
//                        s.collideWith(bullet, hit);
//}
//                }
//
//            }
//              if (hit.size() > 0) {
//                  
//              }


            }
        }
        if (pausedShootingCounting) {
            gunShootWait = (float) Math.random() * 40;
            pausedShootingCounting = false;
            pausedShooting = false;

        }
        if (gunShootWait <= 0) {
            pausedShooting = true;
            pausedShootingCounting = true;
        }
        gunShootWait -= 5 * tpf;
        shootTime -= tpf * ROF;
        //  System.out.println(gunShootWait);
    }
    /* if enemy can see player, find the closest coverpoint with a Line of Sight
     * if enemy can see player and no CP has Line of sight, shoot and move to closest wanderpoint
     * if enemy can't see player, pick wanderpoint with highest score
     * 
     * 
     * 
     */

    public void findNextPoint() {
        //navi.clearPath();

        LOSCoverpoints.clear();
        LOSpoints.clear();

        if (canSeePlayer) { //if an enemy is in view
            LOSCoverpoints.clear();
            LOSpoints.clear();

            //navi.clearPath();
            //Shoot();
            STATE = "Covering";


            for (CoverPoint c : coverPoint) { //adds cover points vith LOS to new Array
                if (c.isLOS()) {
                    LOSpoints.add(c);
                }
            }
            for (CoverPoint d : LOSpoints) { //adds coverpoints with LOS and cover form enemy in Array
                if (d.isCover()) {
                    LOSCoverpoints.add(d);
                }
            }

            if (LOSCoverpoints.size() > 0) { //if their are multiple CP options
                // System.out.println(" numOfLOSpoints " + LOSCoverpoints.size());
                CoverPoint closest = null;
                float closestdistance = 9999;
                float distance = 0;
                float distance2 = 0;
                for (CoverPoint d : LOSCoverpoints) { //finds the distance to each coverpoint
                    distance = Soldier.getWorldTranslation().distance(d.getWorldTranslation());
                    distance2 = d.getWorldTranslation().distance(playerLoc);//from cp to player
                    if (distance < closestdistance) {
                        //if (distance2 > 5) {
                        closestdistance = distance;
                        closest = d;

                        //}
                        //else{
                        //closest = currentpoint;
                        //}
                    }
                }
                //move to CP with closest distance here and TODO: highest score 
                //if (closest != null) {
                currentpoint = closest;
          //      if (closest.getWorldTranslation() != navi.getPath().getLast().getPosition()) {
          //          navi.clearPath();
          //          navi.setPosition(Soldier.getWorldTranslation());
           //         navi.computePath(closest.getWorldTranslation());
                    System.out.println("RECALCULATING PATH: Moving to best CoverPoint");
                    // System.out.println("Current LOS " + closest.isLOS() + "Current COver " + closest.isCover());
                    //System.out.println(" target " + navi.getPath().getLast());
            //    }


                // move = true;

            }
            if (LOSpoints.isEmpty()) { //if their are no ideal coverpoints with LOS
                CoverPoint closest = null;
                float closestdistance = 9999;
                for (CoverPoint w : coverPoint) { //find closest CoverPoint here... (FOR NOW)
                    float distance = Soldier.getWorldTranslation().distance(w.getWorldTranslation());

                    if (distance < closestdistance) {
                        closestdistance = distance;
                        closest = w;
                    }
                }
                //move to closets CP
                if (closest != null) {
                    currentpoint = closest;
//                    navi.clearPath();
        //            navi.setPosition(Soldier.getWorldTranslation());
//                    if (closest.getWorldTranslation() != navi.getPath().getLast().getPosition()){
        //            navi.computePath(closest.getWorldTranslation());

                    System.out.println(" moving to closest CP ");
                    System.out.println("RECALCULATING PATH: Moving to closest CoverPoint");

                }
            }
        } else { //if can't see player
            if (lastPlayerLoc != null) { //if knows the lastlocation it saw the player
                if (lastUsedPlayerLoc != lastPlayerLoc) { //if the last known location is the same
                    lastUsedPlayerLoc.set(lastPlayerLoc);
//                    navi.clearPath();
          //          navi.setPosition(Soldier.getWorldTranslation());
                    //navi.computePath(new Vector3f(lastPlayerLoc.x, 0, lastPlayerLoc.z));
          //          navi.computePath(lastPlayerLoc);
                    chasing = true;
                    System.out.println("RECALCULATING PATH: Moving to last Seen PlayerLoc");
                    //System.out.println("LastPlayerLoc " + new Vector3f(lastPlayerLoc.x, 0, lastPlayerLoc.z));


                }
            }
            if (lastPlayerLoc == null) { // it hasnt seen another player
                setNewDestination();
            }
            //   moveToWanderPoints();
        }
    }

    public void setPlayerLoc(Vector3f loc) {
        playerLoc = loc;
    }

    private void moveToDestination(float tpf) {

//        Path.Waypoint wayPoint = navi.getNextWaypoint();

  //      if (wayPoint == null) {
      //      return;
  //      }
        Vector3f vector = null; //wayPoint.getPosition().subtract(Soldier.getWorldTranslation());
      Vector3f loc = null;//wayPoint.getPosition();
        if (vector.length() > 1) { //if the distance to the waypoint is greater than 1
            // Ai.move(vector.normalizeLocal().multLocal(tpf * FollowMovementSpeed));
            Soldier.move(vector.normalizeLocal().multLocal(tpf * FollowMovementSpeed));
            running = true;
            // System.out.println("Running");
            if (!canSeePlayer) {
                Soldier.lookAt(new Vector3f(loc.x, 0, loc.z), Vector3f.UNIT_Y);
            } else {
                Soldier.lookAt(new Vector3f(playerLoc.x, 0, playerLoc.z), Vector3f.UNIT_Y);
            }

            return;

   //     } else if (!navi.isAtGoalWaypoint()) {
            //atDestination = false;
           // navi.goToNextWaypoint();
   //     }
 //       if (navi.isAtGoalWaypoint()) {

            //atDestination = true;

          //  if (canSeePlayer) { //if taking cover
             //   covering = true;
            }
            if (chasing && !canSeePlayer) { //if it did not find player after chasing
              //  setNewDestination();
              //  chasing = false;
               // System.out.println("Arrived to LastPlayerLoc and can't see player");
            }

    //    }

    }
    int oldWanderPoint = 1000;

    public void setNewDestination() {
      //  navi.clearPath();
        Random ran = new Random();
        int x = ran.nextInt(3);
        //int x = 4;

        if (oldWanderPoint != x) { //if the new Point is different that the last one

            oldWanderPoint = x;
       //     navi.setPosition(Soldier.getWorldTranslation());
            if (WanderPoint.get(x) != null) {
       //         navi.computePath(WanderPoint.get(x).getWorldTranslation());
            }

        }
        if (oldWanderPoint == x) { //if the new Point is the same as the last one, find a new one
            while (x == oldWanderPoint) {
                x = ran.nextInt(3);
            }

            if (WanderPoint.get(x) != null) {
        //        navi.setPosition(Soldier.getWorldTranslation());
        //        navi.computePath(WanderPoint.get(x).getWorldTranslation());
            }

        }
    }
    int FieldofFiew = 120;
    CollisionResults results;
    Ray sight = new Ray();

    private void SeeEnemy() {
        Vector3f enemyDirection = Soldier.getWorldRotation().mult(new Vector3f(0, 0, 1)).normalize();
        Vector3f DirectionToPlayer = playerLoc.subtract(Soldier.getWorldTranslation()).normalize();
        DirectionToPlayer.y = 0;
        float angle = enemyDirection.angleBetween(DirectionToPlayer);
        float angleABS = Math.abs(angle);
        double angleee = Math.toDegrees(angleABS);
        if (angleee > FieldofFiew / 2) { // if angle to player is too big
            canSeePlayer = false;
            //return false;
        } else if (angleee <= FieldofFiew / 2) { // if angle withing FOV
            sight.setOrigin(new Vector3f(Soldier.getWorldTranslation().x, Soldier.getWorldTranslation().y + 6, Soldier.getWorldTranslation().z));
            sight.setDirection(DirectionToPlayer);
            results = new CollisionResults();
            Node rootNode = this.getParent();
            Node collide = new Node();
            for (Spatial s : rootNode.getChildren()) {
                //System.out.println(s.getName());
                if (!(s instanceof Enemy)) {
                    if (!(s instanceof ParticleEmitter)) {

                        s.collideWith(sight, results); //cast ray to see if 
                        //enemy can actually see player
                    }
                }

            }
            if (results.size() > 0) {

                String e = results.getClosestCollision().getGeometry().getUserData("Player");
                if (e != null) { //if clear sight to player
                    canSeePlayer = true;
                    results.clear();
                    // return true;

                } else { // if no sight to player
                    canSeePlayer = false;
                    //return false;
                }
            }
        }
        //return false;

    }

    public void Animations() {
        if (covering) {
            if (!"Crouch".equals(channel.getAnimationName())) {
                channel.setAnim("Crouch", .2f);
                channel.setLoopMode(LoopMode.DontLoop);
                channel.setSpeed(1);
                //System.out.println("Changing to crouch Anim");
            }
            covering = false;
        }
        if (running) {
            if (!"Action.001".equals(channel.getAnimationName())) {
                channel.setAnim("Action.001", .2f);
                channel.setLoopMode(LoopMode.Loop);
                channel.setSpeed(1);
                running = false;
                //System.out.println("Changing to run Anim");
            }
        }
    }

    public void die() {
        //TODO: //activate ragdoll here
        // navi.clearPath();
        findspawnPoint();
        //rootNode.detachChild(this);
    }

    public void findspawnPoint() {
        //TODO: find point
        //spawn();
        int sp = Range(0, SpawnPoint.size());
        //Soldier.setLocalTranslation(SpawnPoint.get(sp).getWorldTranslation());
        
        //navi.setPosition(Soldier.getWorldTranslation());
        //setNewDestination();
        canSeeEnemy = false;
        Health = 100;
        //  Patrol();

    }

    public int Range(int Min, int Max) {
        return Min + (int) (Math.random() * ((Max - Min)));
    } // returns in within range

    private void Shoot() {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void setPoints(ArrayList<CoverPoint> coverPoint, ArrayList<WanderPoint> WanderPoint, ArrayList<Node> SpawnPoint) {
        this.coverPoint = coverPoint;
        this.WanderPoint = WanderPoint;
        this.SpawnPoint = SpawnPoint;
        findspawnPoint();

    }

    @Override
    public void updateModelBound() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setModelBound(BoundingVolume modelBound) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getVertexCount() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getTriangleCount() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Spatial deepClone() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void depthFirstTraversal(SceneGraphVisitor visitor) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void breadthFirstTraversal(SceneGraphVisitor visitor, Queue<Spatial> queue) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private void setUpSightControl() {
    }

    @Override
    public void onAnimCycleDone(AnimControl control, AnimChannel channel, String animName) {
    }

    @Override
    public void onAnimChange(AnimControl control, AnimChannel channel, String animName) {
    }
}
